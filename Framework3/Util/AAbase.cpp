//_________________________________________________________//
// File: AAbase.cpp										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: AAbase class functions					   //
//_________________________________________________________//

#include "AAbase.h"

AAbase::AAbase(Renderer* renderer, const std::string shader)
{
	p_renderer = renderer;

	if ((m_shader = renderer->addShader((c_shaderPath + shader).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", shader.c_str());

		ErrorMsg(str);
	}
}

AAbase::~AAbase()
{

}
