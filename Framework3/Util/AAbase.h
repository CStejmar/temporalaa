//_________________________________________________________//
// File: AAbase.h										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: AAbase class declaration			       //
//_________________________________________________________//

#ifndef _AABASE_H_
#define _AABASE_H_

#include "../Renderer.h"

#include <string>

class AAbase {

public:
	// Constructor and destructor
	AAbase(Renderer* renderer, const std::string shader);
	~AAbase();

protected:
	ShaderID m_shader;
	const std::string c_shaderPath = "../../Shaders/";

	Renderer* p_renderer;

};

#endif // _AAbase_H_