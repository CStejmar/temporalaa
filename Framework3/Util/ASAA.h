//_________________________________________________________//
// File: ASAA.h										       //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: ASAA class declaration			    	   //
//_________________________________________________________//

#ifndef _ASAA_H_
#define _ASAA_H_

#include "../Renderer.h"
#include "../Direct3D11/Direct3D11Renderer.h"
#include "SubpixelBuffer.h"
#include "DynamicModel.h"

#include <string>
#include <vector>

class ASAA {

public:
	// Constructor and destructor
	ASAA(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane, mat4 projectionMatrix);
	~ASAA();

	std::vector<SubpixelBuffer> subpixelBuffers;

	// Functions
	void resize(const int w, const int h);
	TextureID getRT();
	void setViewMatrix(mat4 matrix);
	void setProjectionOffsetMatrix(mat4 matrix);
	void reset();
	void resetFrame();
	float2 getGaussianOffset(int seed);
	int getIndex();
	void setIndex(int index);
	void apply(TextureID currentFrame, ID3D11DeviceContext* context, TextureID depthRT, TextureID velocityTexture, DepthStateID depthState, RasterizerStateID rasterizerState, float3 camPos, int width, int height);
	void applyWVP(DynamicModel* dynamicModel, TextureID currentFrame, ID3D11DeviceContext* context, TextureID depthRT, DepthStateID depthState, RasterizerStateID rasterizerState, float3 camPos, int width, int height);

private:

	TextureID m_FrameRT, m_TempSubpixelRT, m_SamplesRT, m_TempSamplesRT, m_PreviousDepthRT, m_TempPreviousDepthRT;

	mat4 m_ProjectionMatrix, m_ProjectionOffsetMatrix, m_ViewMatrix;

	float2 m_LinearDepthParams;
	float4 m_InvFar;
	int m_Index;

	ShaderID m_shader, m_depthShader, m_shaderWVP;
	SamplerStateID m_Filter;

	const std::string c_shaderPath = "../../Shaders/ASAA.shd";
	const std::string c_depthShaderPath = "../../Shaders/PreviousDepth.shd";
	const std::string c_shaderWVPPath = "../../Shaders/ASAAwvp.shd";

	Renderer* p_renderer;
};

#endif // _ASAA_H_