//_________________________________________________________//
// File: ASAAexp.cpp									   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: ASAAexp class functions					   //	
//_________________________________________________________//

#include "ASAAexp.h"

ASAAexp::ASAAexp(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane, mat4 projectionMatrix)
{
	p_renderer = renderer;
	m_LinearDepthParams = linearDepthParams;
	m_InvFar = float4(1.0f / farPlane, 0.0f, 0.0f, 0.0f);
	m_Index = 0;
	first = true;

	m_ProjectionMatrix = projectionMatrix;
	m_ProjectionOffsetMatrix = identity4();
	m_ViewMatrix = identity4();

	if ((m_shader = renderer->addShader(c_shaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_shaderPath.c_str());

		ErrorMsg(str);
	}

	// Final frame render target
	if ((m_FrameRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	if ((m_TempSubpixelRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	// Number of samples per pixel render target
	if ((m_SamplesRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA16UI, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	if ((m_TempSamplesRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA16UI, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	if ((m_PreviousDepthRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA16F, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	// Subpixel frames render targets
	for (uint i = 0; i < 4; i++)
	{
		SubpixelBuffer sb = SubpixelBuffer(renderer, projectionMatrix, width, height);
		subpixelBuffers.push_back(sb);
	}

	// Bilinear sampler for depth test
	if ((m_Filter = renderer->addSamplerState(BILINEAR, CLAMP, CLAMP, CLAMP)) == SS_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add sampler state");

		ErrorMsg(str);
	}

}

ASAAexp::~ASAAexp()
{
	subpixelBuffers.erase(subpixelBuffers.begin(), subpixelBuffers.end());
}

void ASAAexp::resize(const int w, const int h)
{
	p_renderer->resizeRenderTarget(m_FrameRT, w, h, 1, 1, 1);

	for (uint i = 0; i < subpixelBuffers.size(); i++)
	{
		p_renderer->resizeRenderTarget(subpixelBuffers.at(i).m_SubpixelRT, w, h, 1, 1, 1);
	}

	p_renderer->resizeRenderTarget(m_SamplesRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_TempSamplesRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_TempSubpixelRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_PreviousDepthRT, w, h, 1, 1, 1);
}

TextureID ASAAexp::getRT()
{
	return m_FrameRT;
}

void ASAAexp::setViewMatrix(mat4 matrix)
{
	m_ViewMatrix = matrix;
}

void ASAAexp::setProjectionOffsetMatrix(mat4 matrix)
{
	m_ProjectionOffsetMatrix = matrix;
}

void ASAAexp::reset()
{
	Direct3D11Renderer *d3D11_renderer = ((Direct3D11Renderer *)p_renderer);

	for (uint i = 0; i < subpixelBuffers.size(); i++)
	{
		subpixelBuffers.at(i).resetBuffer(d3D11_renderer);
	}

	d3D11_renderer->clearRenderTarget(m_SamplesRT, float4(0.0f, 0.0f, 0.0f, 0.0f));
	d3D11_renderer->clearRenderTarget(m_PreviousDepthRT, float4(0.0f, 0.0f, 0.0f, 0.0f));
	//m_FrameRT, m_TempSamplesRT, m_TempSubpixelRT does not have to be reset because they are always overwritten

	m_Index = 0;
	first = true;
}

void ASAAexp::resetFrame()
{
	Direct3D11Renderer *d3D11_renderer = ((Direct3D11Renderer *)p_renderer);

	d3D11_renderer->clearRenderTarget(m_FrameRT, float4(0.0f, 0.0f, 0.0f, 0.0f));
}

// Gaussian random number by Marsaglia polar method (Version of Box�Muller transform)
double ASAAexp::getGaussianOffset(int seed)
{
	double mean = 0.0;
	double variance = 0.2575*0.2575; // standard deviation from "Amortized supersampling"

	srand(seed);

	static bool hasSpare = false;
	static double spare;

	if (hasSpare)
	{
		hasSpare = false;
		return mean + variance * spare;
	}

	hasSpare = true;
	static double u, v, s;
	do
	{
		u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
		v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
		s = u * u + v * v;
	} while ((s >= 1.0) || (s == 0.0));

	s = sqrt(-2.0 * log(s) / s);
	spare = v * s;
	return mean + variance * u * s;
}

int ASAAexp::getIndex()
{
	return m_Index;
}

void ASAAexp::setIndex(int index)
{
	m_Index = index;
}

void ASAAexp::apply(TextureID currentFrame, ID3D11DeviceContext* context, TextureID depthRT, DepthStateID depthState, RasterizerStateID rasterizerState, float3 camPos, int width, int height)
{
	TextureID RTs[] = { m_FrameRT, m_TempSubpixelRT, m_TempSamplesRT };
	p_renderer->changeRenderTargets(RTs, elementsOf(RTs), TEXTURE_NONE);

	p_renderer->reset();
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(m_shader);
	p_renderer->setRasterizerState(rasterizerState);

	// Eliminate translation from view matrix and work in local space to preserve precision
	mat4 InvViewProjMatrix = m_ProjectionOffsetMatrix*m_ViewMatrix;
	InvViewProjMatrix.translate(camPos); // Removes translation. This is then added again in shader as EyePosition
	InvViewProjMatrix = !InvViewProjMatrix;

	p_renderer->setShaderConstant4x4f("ViewProjInv", InvViewProjMatrix);
	p_renderer->setShaderConstant4f("InvFar", m_InvFar);

	p_renderer->setSamplerState("BilinearSampler", m_Filter);
	p_renderer->setTexture("CurrentFrame", currentFrame);

	// Subpixel buffers
	p_renderer->setTexture("SubpixelBuffer0", subpixelBuffers.at(0).m_SubpixelRT);
	p_renderer->setTexture("SubpixelBuffer1", subpixelBuffers.at(1).m_SubpixelRT);
	p_renderer->setTexture("SubpixelBuffer2", subpixelBuffers.at(2).m_SubpixelRT);
	p_renderer->setTexture("SubpixelBuffer3", subpixelBuffers.at(3).m_SubpixelRT);

	p_renderer->setTexture("DepthTexture", depthRT);
	p_renderer->setTexture("SamplesTexture", m_SamplesRT);

	mat4 subpixelViewProjMatrix[5] = { subpixelBuffers.at(0).getViewProjectionMatrix(), subpixelBuffers.at(1).getViewProjectionMatrix(), subpixelBuffers.at(2).getViewProjectionMatrix(), subpixelBuffers.at(3).getViewProjectionMatrix(), m_ProjectionMatrix*m_ViewMatrix };
	mat4 subpixelViewProjOffsetMatrix[5];

	subpixelViewProjOffsetMatrix[0] = m_ProjectionOffsetMatrix*subpixelBuffers.at(0).getViewMatrix();
	subpixelViewProjOffsetMatrix[1] = m_ProjectionOffsetMatrix*subpixelBuffers.at(1).getViewMatrix();
	subpixelViewProjOffsetMatrix[2] = m_ProjectionOffsetMatrix*subpixelBuffers.at(2).getViewMatrix();
	subpixelViewProjOffsetMatrix[3] = m_ProjectionOffsetMatrix*subpixelBuffers.at(3).getViewMatrix();
	subpixelViewProjOffsetMatrix[4] = m_ProjectionOffsetMatrix*m_ViewMatrix;

	p_renderer->setShaderConstantArray4x4f("SubpixelViewProjMatrix", subpixelViewProjMatrix, 5);
	p_renderer->setShaderConstantArray4x4f("SubpixelViewProjOffsetMatrix", subpixelViewProjOffsetMatrix, 5);

	p_renderer->setShaderConstant3f("EyePosition", camPos);
	p_renderer->setShaderConstant2f("LinearDepthParams", m_LinearDepthParams);
	p_renderer->setShaderConstant1i("Index", m_Index);
	p_renderer->setShaderConstant2f("TextureSize", float2((float)width, (float)height));

	p_renderer->apply();

	p_renderer->drawArrays(PRIM_TRIANGLES, 0, 3);

	subpixelBuffers.at(m_Index).setViewMatrix(m_ViewMatrix);
	// Update a subpixel buffer. This is done because otherwise we will have a subpixelBuffer bound as render target while using it in the shader
	context->CopyResource(((Direct3D11Renderer*)p_renderer)->getResource(subpixelBuffers.at(m_Index).m_SubpixelRT), ((Direct3D11Renderer*)p_renderer)->getResource(m_TempSubpixelRT));
	context->CopyResource(((Direct3D11Renderer*)p_renderer)->getResource(m_SamplesRT), ((Direct3D11Renderer*)p_renderer)->getResource(m_TempSamplesRT));
}