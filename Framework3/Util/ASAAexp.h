//_________________________________________________________//
// File: ASAAexp.h										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: ASAAexp class declaration				   //
//_________________________________________________________//

#ifndef _ASAAEXP_H_
#define _ASAAEXP_H_

#include "../Renderer.h"
#include "../Direct3D11/Direct3D11Renderer.h"
#include "SubpixelBuffer.h"
#include "DynamicModel.h"

#include <string>
#include <vector>

class ASAAexp {

public:
	// Constructor and destructor
	ASAAexp(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane, mat4 projectionMatrix);
	~ASAAexp();

	std::vector<SubpixelBuffer> subpixelBuffers;

	// Functions
	void resize(const int w, const int h);
	TextureID getRT();
	void setViewMatrix(mat4 matrix);
	void setProjectionOffsetMatrix(mat4 matrix);
	void reset();
	void resetFrame();
	double getGaussianOffset(int seed);
	int getIndex();
	void setIndex(int index);
	void apply(TextureID currentFrame, ID3D11DeviceContext* context, TextureID depthRT, DepthStateID depthState, RasterizerStateID rasterizerState, float3 camPos, int width, int height);

private:

	TextureID m_FrameRT, m_TempSubpixelRT, m_SamplesRT, m_TempSamplesRT, m_PreviousDepthRT;

	mat4 m_ProjectionMatrix, m_ProjectionOffsetMatrix, m_ViewMatrix;

	float2 m_LinearDepthParams;
	float4 m_InvFar;

	int m_Index;
	bool first;

	ShaderID m_shader;
	SamplerStateID m_Filter;

	const std::string c_shaderPath = "../../Shaders/ASAAexp.shd";

	Renderer* p_renderer;
};

#endif // _ASAAEXP_H_