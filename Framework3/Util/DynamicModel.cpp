//_________________________________________________________//
// File: DynamicModel.cpp								   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: DynamicModel class functions			   //
//_________________________________________________________//

#include "DynamicModel.h"

DynamicModel::DynamicModel(const std::string modelName, vec3 position, vec3 rotation, vec3 size) : StaticModel(modelName, position, rotation, size)
{
	m_velocity = identity4();
	m_rotation = identity4();
	m_prevPlacement = identity4();
}

DynamicModel::DynamicModel(const std::string modelName, bool removeNormals, bool tangent, vec3 position, vec3 rotation, vec3 size) : StaticModel(modelName,removeNormals,tangent,position,rotation,size)
{
	m_velocity = identity4();
	m_rotation = identity4();
	m_prevPlacement = identity4();
}

DynamicModel::DynamicModel(Model* model, const std::string modelName, vec3 position, vec3 rotation, vec3 size) : StaticModel(model, modelName, position, rotation, size)
{
	m_velocity = identity4();
	m_rotation = identity4();
	m_prevPlacement = identity4();
	m_prevWorldMatrix[0] = identity4();
	m_prevWorldMatrix[1] = identity4();
	m_prevWorldMatrix[2] = identity4();
	m_prevWorldMatrix[3] = identity4();
}

DynamicModel::~DynamicModel()
{

}

void DynamicModel::setVelocity(mat4 velocity)
{
	m_velocity = velocity;
}
void DynamicModel::setRotation(mat4 rotation)
{
	m_rotation = rotation;
}

mat4 DynamicModel::getPrevPlacement()
{
	return m_prevPlacement;
}

mat4 DynamicModel::getPrevWorldMatrix(int index)
{
	return m_prevWorldMatrix[index];
}

void DynamicModel::updatePlacement(int index)
{
	m_prevWorldMatrix[index] = m_placement;

	m_prevPlacement = m_placement;
	mat4 totalTranslation = translate(vec3(m_placement.rows[0].w, m_placement.rows[1].w, m_placement.rows[2].w) + vec3(m_velocity.rows[0].w, m_velocity.rows[1].w, m_velocity.rows[2].w));
	
	m_placement = totalTranslation*scale(m_InitSize.x, m_InitSize.y, m_InitSize.z)*m_rotation*rotateZXY(m_InitRotation.x, m_InitRotation.y, m_InitRotation.z);
}