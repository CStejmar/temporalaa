//_________________________________________________________//
// File: DynamicModel.h								       //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: DynamicModel class declaration			   //
//_________________________________________________________//

#ifndef _DYNAMICMODEL_H_
#define _DYNAMICMODEL_H_

#include "StaticModel.h"
#include "../Math/Vector.h"
#include <string>

class DynamicModel : public StaticModel{

public:
	// Constructor and destructor
	DynamicModel(const std::string modelName, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	DynamicModel(const std::string modelName, bool removeNormals, bool tangent, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	DynamicModel(Model* model, const std::string modelName, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	~DynamicModel();

	// Dynamic functions
	void setVelocity(mat4 velocity);
	void setRotation(mat4 rotation);
	mat4 getPrevPlacement();
	mat4 getPrevWorldMatrix(int index);
	void updatePlacement(int index);

protected:

	// Dynamic properties
	mat4 m_velocity;
	mat4 m_rotation;
	mat4 m_prevPlacement; // (prevWorld matrix)
	mat4 m_prevWorldMatrix[4];
};

#endif // _DYNAMICMODEL_H_