//_________________________________________________________//
// File: FXAA.cpp								           //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: FXAA class functions			           //
//_________________________________________________________//

#include "FXAA.h"

FXAA::FXAA(Renderer* renderer, const std::string shader, const std::string shaderDebug) : AAbase(renderer, shader)
{
	m_FXAASubPixelSharpness = 0.26f; // Initial value from JC3

	if ((m_shaderDebug = renderer->addShader((c_shaderPath + shaderDebug).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", shaderDebug.c_str());

		ErrorMsg(str);
	}
}

FXAA::~FXAA()
{

}

void FXAA::increaseSubPixelSharpness()
{
	m_FXAASubPixelSharpness += 0.01f;
	if (m_FXAASubPixelSharpness > 1.0f)
		m_FXAASubPixelSharpness = 1.0f;
}

void FXAA::decreaseSubPixelSharpness()
{
	m_FXAASubPixelSharpness -= 0.01f;
	if (m_FXAASubPixelSharpness < 0.0f)
		m_FXAASubPixelSharpness = 0.0f;
}

void FXAA::apply(TextureID texture, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp)
{
	p_renderer->changeToMainFramebuffer();

	p_renderer->reset();
	p_renderer->setDepthState(depthState);

	if (debugDraw)
		p_renderer->setShader(m_shaderDebug);
	else
		p_renderer->setShader(m_shader);

	p_renderer->setRasterizerState(rasterizerState);

	float inv_w = 1.0f / width;
	float inv_h = 1.0f / height;

	const vec4 vs_params = vec4(0.5f * inv_w, 0.5f * inv_h, 0, 0);
	p_renderer->setShaderConstantArray4f("VsConsts", &vs_params, 1);

	// Fragment shader
	vec4 fs_params[4] =
	{
		vec4( -m_FXAASubPixelSharpness * inv_w, -m_FXAASubPixelSharpness * inv_h, m_FXAASubPixelSharpness * inv_w, m_FXAASubPixelSharpness * inv_h ),
		vec4( -2.0f * inv_w, -2.0f * inv_h, 2.0f * inv_w, 2.0f * inv_h ),
		vec4( 8.0f * inv_w, 8.0f * inv_h, -4.0f * inv_w, -4.0f * inv_h ),
		vec4(1.0f, -1.0f, 0.25f, -0.25f ),
	};

	p_renderer->setShaderConstantArray4f("PsConsts", &fs_params[0], 4);

	p_renderer->setTexture("Tex", texture);
	p_renderer->setSamplerState("Samp", linearClamp);

	p_renderer->apply();

	p_renderer->drawArrays(PRIM_TRIANGLES, 0, 3);
}