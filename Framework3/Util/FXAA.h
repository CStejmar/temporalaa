//_________________________________________________________//
// File: FXAA.h								               //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: FXAA class declaration					   //
//_________________________________________________________//


#ifndef _FXAA_H_
#define _FXAA_H_

#include "AAbase.h"

class FXAA : AAbase {

public:
	// Constructor and destructor
	FXAA(Renderer* renderer, const std::string shader, const std::string shaderDebug);
	~FXAA();

	// Functions
	void increaseSubPixelSharpness();
	void decreaseSubPixelSharpness();
	void apply(TextureID texture, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp);

private:
	float m_FXAASubPixelSharpness;
	ShaderID m_shaderDebug;
};

#endif // _FXAA_H_