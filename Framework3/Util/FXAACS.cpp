//_________________________________________________________//
// File: FXAACS.cpp								           //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: FXAACS (compute shader) class functions	   //
//_________________________________________________________//

#include "FXAACS.h"

FXAACS::FXAACS(Renderer* renderer, const std::string pass1Shd, int width, int height) : AAbase(renderer, pass1Shd)
{
	if ((m_resolvePassShd = renderer->addShader(c_resolvePassShaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_resolvePassShaderPath.c_str());

		ErrorMsg(str);
	}
	if ((m_pass2HShd = renderer->addShader(c_pass2HShaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_pass2HShaderPath.c_str());

		ErrorMsg(str);
	}
	if ((m_pass2VShd = renderer->addShader(c_pass2VShaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_pass2VShaderPath.c_str());

		ErrorMsg(str);
	}
	// Debug shaders
	if ((m_pass2HDebugShd = renderer->addShader(c_pass2HDebugShaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_pass2HDebugShaderPath.c_str());

		ErrorMsg(str);
	}
	if ((m_pass2VDebugShd = renderer->addShader(c_pass2VDebugShaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_pass2VDebugShaderPath.c_str());

		ErrorMsg(str);
	}

	m_contrastThreshold = 0.2f; // Initial value from MM
	m_subpixelRemoval = 0.75f; // Initial value from MM

	b_paramSwitch = false; // Initial value

	if ((m_HWorkUAV = renderer->addTextureBuffer(width*height * 16, 16, DEFAULT, false, true, false, NULL, USE_SRV | USE_UAV)) == TB_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture buffer");

		ErrorMsg(str);
	}

	if ((m_VWorkUAV = renderer->addTextureBuffer(width*height * 16, 16, DEFAULT, false, true, false, NULL, USE_SRV | USE_UAV)) == TB_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture buffer");

		ErrorMsg(str);
	}

	if ((m_IndirectArgsUAV = renderer->addTextureBuffer(6 * sizeof(uint), sizeof(uint), DEFAULT, false, false, true, NULL, USE_UAV | INDIRECT_ARGS)) == TB_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture buffer");

		ErrorMsg(str);
	}

	if ((m_LumaTextureUAV = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_R8, 1, SS_NONE, USE_UAV)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}
}

FXAACS::~FXAACS()
{

}

void FXAACS::increaseContrastThreshold()
{
	m_contrastThreshold += 0.01f;
	if (m_contrastThreshold > 1.0f)
		m_contrastThreshold = 1.0f;
}

void FXAACS::decreaseContrastThreshold()
{
	m_contrastThreshold -= 0.01f;
	if (m_contrastThreshold < 0.0f)
		m_contrastThreshold = 0.0f;
}

void FXAACS::increaseSubpixelRemoval()
{
	m_subpixelRemoval += 0.01f;
	if (m_subpixelRemoval > 1.0f)
		m_subpixelRemoval = 1.0f;
}

void FXAACS::decreaseSubpixelRemoval()
{
	m_subpixelRemoval -= 0.01f;
	if (m_subpixelRemoval < 0.0f)
		m_subpixelRemoval = 0.0f;
}

void FXAACS::resize(const int w, const int h)
{
	p_renderer->resizeRenderTarget(m_HWorkUAV, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_VWorkUAV, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_IndirectArgsUAV, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_LumaTextureUAV, w, h, 1, 1, 1);
}

void FXAACS::apply(TextureID texture, ID3D11DeviceContext* context, ID3D11Texture2D* backBuffer, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp)
{
	p_renderer->changeRenderTargets(NULL, 0, TEXTURE_NONE);

	// FXAACS PASS 1
	p_renderer->reset();
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(m_shader);
	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setUAVBuffer("HWork", m_HWorkUAV, 0);
	p_renderer->setUAVBuffer("VWork", m_VWorkUAV, 0);
	p_renderer->setTexture("Color", texture);
	p_renderer->setUAVTexture("Luma", m_LumaTextureUAV);
	p_renderer->setShaderConstant1f("ContrastThreshold", m_contrastThreshold);
	p_renderer->setShaderConstant1f("SubpixelRemoval", m_subpixelRemoval);

	p_renderer->apply();

	int gridX = ((width - 1) / 8) + 1;
	int gridY = ((height - 1) / 8) + 1;

	context->Dispatch(gridX, gridY, 1); // this flushes all compute

	//// FXAACS PASS 2: Resolve work
	p_renderer->reset();
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(m_resolvePassShd);
	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setUAVBuffer("WorkQueueH", m_HWorkUAV);
	p_renderer->setUAVBuffer("WorkQueueV", m_VWorkUAV);
	p_renderer->setUAVBuffer("IndirectParams", m_IndirectArgsUAV);

	p_renderer->apply();

	context->Dispatch(1, 1, 1); //writes 6 uint values in the dispatch buffer

	// Clear UAVs
	p_renderer->reset();
	p_renderer->applyUAVs();

	float2 invWH = float2(1.0f / (float)width, 1.0f / (float)height);

	// FXAACS PASS 2: Horizontal
	p_renderer->reset();
	p_renderer->setDepthState(depthState);

	if (debugDraw)
		p_renderer->setShader(m_pass2HDebugShd);
	else
	p_renderer->setShader(m_pass2HShd);

	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setTexture("Luma", m_LumaTextureUAV);
	p_renderer->setBuffer("WorkQueue", m_HWorkUAV);
	p_renderer->setUAVTexture("Color", texture);
	p_renderer->setSamplerState("LinearSampler", linearClamp);
	p_renderer->setShaderConstant2f("RcpTextureSize", invWH);
	
	p_renderer->apply();

	context->DispatchIndirect(p_renderer->getTextureBuffer(m_IndirectArgsUAV), 0);

	// FXAACS PASS 2: Vertical
	p_renderer->reset();
	p_renderer->setDepthState(depthState);
	
	if (debugDraw)
		p_renderer->setShader(m_pass2VDebugShd);
	else
		p_renderer->setShader(m_pass2VShd);

	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setTexture("Luma", m_LumaTextureUAV);
	p_renderer->setBuffer("WorkQueue", m_VWorkUAV);
	p_renderer->setUAVTexture("Color", texture);
	p_renderer->setSamplerState("LinearSampler", linearClamp);
	p_renderer->setShaderConstant2f("RcpTextureSize", invWH);
	
	p_renderer->apply();

	context->DispatchIndirect(p_renderer->getTextureBuffer(m_IndirectArgsUAV), 12); //implicit flush here too

	context->CopyResource(backBuffer, ((Direct3D11Renderer*)p_renderer)->getResource(texture));
	p_renderer->changeToMainFramebuffer();	
}