//_________________________________________________________//
// File: FXAACS.h								           //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: FXAACS (compute shader) class declaration  //
//_________________________________________________________//

#ifndef _FXAACS_H_
#define _FXAACS_H_

#include "AAbase.h"
#include "../Direct3D11/Direct3D11Renderer.h"

class FXAACS : AAbase {

public:
	// Constructor and destructor
	FXAACS(Renderer* renderer, const std::string shader, int width, int height);
	~FXAACS();

	bool b_paramSwitch;

	// Functions
	void increaseContrastThreshold();
	void decreaseContrastThreshold();

	void increaseSubpixelRemoval();
	void decreaseSubpixelRemoval();

	void resize(const int w, const int h);

	void apply(TextureID texture, ID3D11DeviceContext* context, ID3D11Texture2D* backBuffer, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp);

private:
	float m_contrastThreshold;
	float m_subpixelRemoval;

	const std::string c_resolvePassShaderPath = "../../Shaders/FXAACS/FXAAResolveWorkQueue.shd";
	const std::string c_pass2HShaderPath = "../../Shaders/FXAACS/FXAAPass2H.shd";
	const std::string c_pass2VShaderPath = "../../Shaders/FXAACS/FXAAPass2V.shd";
	const std::string c_pass2HDebugShaderPath = "../../Shaders/FXAACS/FXAAPass2HDebug.shd";
	const std::string c_pass2VDebugShaderPath = "../../Shaders/FXAACS/FXAAPass2VDebug.shd";

	ShaderID m_resolvePassShd, m_pass2HShd, m_pass2VShd;
	ShaderID m_pass2HDebugShd, m_pass2VDebugShd;

	TextureID m_LumaTextureUAV;
	TextureBufferID m_HWorkUAV, m_VWorkUAV, m_IndirectArgsUAV;

};

#endif // _FXAACS_H_