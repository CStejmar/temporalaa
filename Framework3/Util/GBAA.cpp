//_________________________________________________________//
// File: GBAA.cpp								           //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: GBAA class functions			           //
//_________________________________________________________//

#include "GBAA.h"

GBAA::GBAA(Renderer* renderer, const std::string shader, const std::string shaderDebug) : AAbase(renderer, shader)
{

	if ((m_shaderDebug = renderer->addShader((c_shaderPath + shaderDebug).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", shaderDebug.c_str());

		ErrorMsg(str);
	}
}

GBAA::~GBAA()
{

}

void GBAA::apply(TextureID texture, TextureID geometryBuffer, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp, SamplerStateID pointClamp)
{
	p_renderer->changeToMainFramebuffer();

	p_renderer->reset();
	p_renderer->setDepthState(depthState);

	if (debugDraw)
		p_renderer->setShader(m_shaderDebug);
	else
		p_renderer->setShader(m_shader);

	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setShaderConstant2f("PixelSize", float2(1.0f / width, 1.0f / height));

	p_renderer->setTexture("BackBuffer", texture);
	p_renderer->setTexture("GeometryBuffer", geometryBuffer);

	p_renderer->setSamplerState("Linear", linearClamp);
	p_renderer->setSamplerState("Point", pointClamp);
	p_renderer->apply();

	p_renderer->drawArrays(PRIM_TRIANGLES, 0, 3);
}