//_________________________________________________________//
// File: GBAA.h								               //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: GBAA class declaration					   //
//_________________________________________________________//

#ifndef _GBAA_H_
#define _GBAA_H_

#include "AAbase.h"

class GBAA : AAbase {

public:
	// Constructor and destructor
	GBAA(Renderer* renderer, const std::string shader, const std::string shaderDebug);
	~GBAA();

	// Functions
	void apply(TextureID texture, TextureID geometryBuffer, int width, int height, bool debugDraw, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID linearClamp, SamplerStateID pointClamp);

private:
	ShaderID m_shaderDebug;
};

#endif // _GBAA_H_