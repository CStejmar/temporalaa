//_________________________________________________________//
// File: StaticModel.cpp								   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-29								   //
// Description: StaticModel class functions			       //
//_________________________________________________________//

#include "StaticModel.h"

StaticModel::StaticModel(const std::string modelName, vec3 position, vec3 rotation, vec3 size)
{
	m_name = modelName;
	p_model = new Model();
	if (!p_model->loadObj((c_modelPath + modelName).c_str()))
	{
		char str[256];
		sprintf(str, "Couldn't open \"%s\"", m_name);
		ErrorMsg(str);
	}

	p_model->removeStream(p_model->findStream(TYPE_NORMAL));
	p_model->computeTangentSpace(true);

	m_InitPosition = position;
	m_InitRotation = rotation;
	m_InitSize = size;

	mat4 translation = identity4();
	translation.translate(position);
	m_placement = translation*scale(size.x, size.y, size.z)*rotateZXY(rotation.x, rotation.y, rotation.z);

	p_shader = nullptr;
	p_filter = nullptr;
	p_renderer = nullptr;
}

StaticModel::StaticModel(const std::string modelName, bool removeNormals, bool tangent, vec3 position, vec3 rotation, vec3 size)
{
	m_name = modelName;
	p_model = new Model();
	if (!p_model->loadObj((c_modelPath + modelName).c_str()))
	{
		char str[256];
		sprintf(str, "Couldn't open \"%s\"", m_name);
		ErrorMsg(str);
	}

	if (removeNormals)
		p_model->removeStream(p_model->findStream(TYPE_NORMAL));

	p_model->computeTangentSpace(tangent);

	m_InitPosition = position;
	m_InitRotation = rotation;
	m_InitSize = size;

	mat4 translation = identity4();
	translation.translate(position);
	m_placement = translation*scale(size.x, size.y, size.z)*rotateZXY(rotation.x, rotation.y, rotation.z);

	p_shader = nullptr;
	p_filter = nullptr;
	p_renderer = nullptr;
}

StaticModel::StaticModel(Model* model, const std::string modelName, vec3 position, vec3 rotation, vec3 size)
{
	m_name = modelName;
	p_model = new Model();
	p_model->copy(model);

	m_InitPosition = position;
	m_InitRotation = rotation;
	m_InitSize = size;

	mat4 translation = identity4();
	translation.translate(position);
	m_placement = translation*scale(size.x, size.y, size.z)*rotateZXY(rotation.x, rotation.y, rotation.z);

	p_shader = nullptr;
	p_filter = nullptr;
	p_renderer = nullptr;
}

StaticModel::~StaticModel()
{
	delete p_model;
}

bool StaticModel::setModelPropertiesVec(const std::vector<std::string> textureNames, const std::vector<std::string> bumpMapNames, ShaderID *shader, Renderer *renderer, SamplerStateID *filter)
{
	p_shader = shader;
	p_renderer = renderer;
	p_filter = filter;

	uint numberOfTextures = textureNames.size();
	textures.assign(numberOfTextures, TEXTURE_NONE);

	uint numberOfBumpMaps = bumpMapNames.size();
	bumpMaps.assign(numberOfBumpMaps, TEXTURE_NONE);

	tile.assign(numberOfTextures, 0);

	for (uint i = 0; i < numberOfTextures; i++)
	{
		if (textureNames.at(i) != "")
			if ((textures.at(i) = p_renderer->addTexture((c_texturePath + textureNames.at(i)).c_str(), true, *p_filter, SRGB)) == TEXTURE_NONE)
				return false;

		// Check whether to tile texture or not
		if (textureNames.at(i).find("tile") != std::string::npos)
			tile.at(i) = 1;
	}

	for (uint i = 0; i < numberOfBumpMaps; i++)
	{
		if (bumpMapNames.at(i) != "")
			if ((bumpMaps.at(i) = p_renderer->addNormalMap((c_texturePath + bumpMapNames.at(i)).c_str(), FORMAT_RGBA8S, true, *p_filter)) == TEXTURE_NONE)
				return false;
	}

	// Upload map to vertex/index buffer
	if (!p_model->makeDrawable(p_renderer, true, *p_shader))
		return false;

	return true;
}

bool StaticModel::setModelProperties(const std::string textureName, const std::string bumpMapName, ShaderID *shader, Renderer *renderer, SamplerStateID *filter, bool texture, bool bumpMap, bool skybox)
{
	p_shader = shader;
	p_renderer = renderer;
	p_filter = filter;

	if (!skybox)
	{
		if (texture)
		{
			TextureID tex;
			if ((tex = p_renderer->addTexture((c_texturePath + textureName).c_str(), true, *p_filter, SRGB)) == TEXTURE_NONE)
				return false;
			else
				textures.push_back(tex);
		}
	}
	else
	{
		const char *files[] =
		{
			"../../Textures/CubeMaps/Cloudy/posx.jpg", "../../Textures/CubeMaps/Cloudy/negx.jpg",
			"../../Textures/CubeMaps/Cloudy/posy.jpg", "../../Textures/CubeMaps/Cloudy/negy.jpg",
			"../../Textures/CubeMaps/Cloudy/posz.jpg", "../../Textures/CubeMaps/Cloudy/negz.jpg",
			/*(c_texturePath + textureName + "posx.jpg").c_str(), (c_texturePath + textureName + "negx.jpg").c_str(),
			(c_texturePath + textureName + "posy.jpg").c_str(), (c_texturePath + textureName + "negy.jpg").c_str(),
			(c_texturePath + textureName + "posz.jpg").c_str(), (c_texturePath + textureName + "negz.jpg").c_str(),*/
		};
		TextureID tex;
		if ((tex = p_renderer->addCubemap(files, true, *p_filter, 1, SRGB)) == TEXTURE_NONE)
			return false;
		else
			textures.push_back(tex);
	}
	if (bumpMap)
	{
		TextureID bumpTex;
		if ((bumpTex = p_renderer->addNormalMap((c_texturePath + bumpMapName).c_str(), FORMAT_RGBA8S, true, *p_filter)) == TEXTURE_NONE)
			return false;
		else
			bumpMaps.push_back(bumpTex);
	}

	// Upload map to vertex/index buffer
	if (!p_model->makeDrawable(p_renderer, true, *p_shader))
		return false;

	return true;
}

void StaticModel::setShader(ShaderID *shader)
{
	p_shader = shader;
}

void StaticModel::setPlacement(vec3 position, vec3 rotation, vec3 size)
{
	mat4 translation = identity4();
	translation.translate(position);
	m_placement = translation*scale(size.x, size.y, size.z)*rotateZXY(rotation.x, rotation.y, rotation.z);

	m_InitPosition = position;
	m_InitRotation = rotation;
	m_InitSize = size;
}

mat4 StaticModel::getPlacement()
{
	return m_placement;
}

vec3 StaticModel::getPosition()
{
	return vec3(m_placement.rows[0].w, m_placement.rows[1].w, m_placement.rows[2].w); // 0,1,2,3 // Correct?
}

vec3 StaticModel::getRotation()
{
	return m_InitRotation;
}

vec3 StaticModel::getScale()
{
	return m_InitSize;
}

Model* StaticModel::getModel()
{
	return p_model;
}

void StaticModel::drawModelVec(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa)
{
	p_renderer->reset();
	p_renderer->setRasterizerState(rasterState);
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(*p_shader);

	p_renderer->setShaderConstant4x4f("modelMatrix", m_placement);
	p_renderer->setShaderConstant4x4f("ViewProj", viewProj);

	if (gbaa)
		p_renderer->setShaderConstant4f("ScaleBias", scaleBias);

	p_renderer->setSamplerState("Filter", *p_filter);

	for (uint i = 0; i < textures.size(); i++) // the bump map vector must have the same size as the texture vector!
	{
		p_renderer->setShaderConstant1i("tile", tile.at(i));
		p_renderer->setTexture("Base", textures.at(i));
		if (bumpMaps.at(i) != TEXTURE_NONE)
			p_renderer->setTexture("Bump", bumpMaps.at(i));

		p_renderer->apply();

		p_model->drawBatch(p_renderer, i);
	}
}

void StaticModel::drawModel(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa, float4 col)
{
	p_renderer->reset();
	p_renderer->setRasterizerState(rasterState);
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(*p_shader);

	if (bumpMaps.size() > 0)
	{
		if (bumpMaps.at(0) != TEXTURE_NONE)
			p_renderer->setTexture("Bump", bumpMaps.at(0));
	}
	if (textures.size() > 0)
	{
		if (textures.at(0) != TEXTURE_NONE)
		{
			p_renderer->setTexture("Base", textures.at(0));
			p_renderer->setSamplerState("Filter", *p_filter);
		}
	}
	else
		p_renderer->setShaderConstant4f("inColor", col);

	if (m_name != "skybox.obj") // Skybox
	{
		p_renderer->setShaderConstant4x4f("modelMatrix", m_placement);

		if (gbaa)
			p_renderer->setShaderConstant4f("ScaleBias", scaleBias);

	}
	p_renderer->setShaderConstant4x4f("ViewProj", viewProj);

	p_renderer->apply();
	p_model->draw(p_renderer);
}

void StaticModel::drawReflectiveModel(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa, vec3 camPos)
{
	p_renderer->reset();
	p_renderer->setRasterizerState(rasterState);
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(*p_shader);

	if (bumpMaps.size() > 0)
	{
		if (bumpMaps.at(0) != TEXTURE_NONE)
			p_renderer->setTexture("Bump", bumpMaps.at(0));
	}
	if (textures.size() > 0)
	{
		if (textures.at(0) != TEXTURE_NONE)
		{
			p_renderer->setTexture("Base", textures.at(0));
			p_renderer->setSamplerState("Filter", *p_filter);
		}
	}

	p_renderer->setShaderConstant4x4f("modelMatrix", m_placement);

	if (gbaa)
		p_renderer->setShaderConstant4f("ScaleBias", scaleBias);

	p_renderer->setShaderConstant4x4f("ViewProj", viewProj);
	p_renderer->setShaderConstant3f("cameraPosition", camPos);

	p_renderer->apply();
	p_model->draw(p_renderer);
}