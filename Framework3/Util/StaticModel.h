//_________________________________________________________//
// File: StaticModel.h								       //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-29								   //
// Description: StaticModel class declaration			   //
//_________________________________________________________//

#ifndef _STATICMODEL_H_
#define _STATICMODEL_H_

#include "Model.h"
//#include "../Math/Vector.h"
#include <string>
#include <vector>

class StaticModel{

public:
	// Constructor and destructor
	StaticModel(const std::string modelName, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	StaticModel(const std::string modelName, bool removeNormals, bool tangent, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	StaticModel(Model* model, const std::string modelName, vec3 position = vec3(0, 0, 0), vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	~StaticModel();

	// Functions
	bool setModelPropertiesVec(const std::vector<std::string> textureNames, const std::vector<std::string> bumpMapNames, ShaderID *shader, Renderer *renderer, SamplerStateID *filter);
	bool setModelProperties(const std::string textureName, const std::string bumpMapName, ShaderID *shader, Renderer *renderer, SamplerStateID *filter, bool texture, bool bumpMap, bool skybox);
	void setShader(ShaderID *shader);

	void drawModelVec(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa);
	void drawModel(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa, float4 color = vec4(0.5f, 0.5f, 0.5f, 1.0f));
	void drawReflectiveModel(DepthStateID depthState, RasterizerStateID rasterState, mat4 viewProj, float4 scaleBias, bool gbaa, vec3 camPos);

	void setPlacement(vec3 position, vec3 rotation = vec3(0, 0, 0), vec3 size = vec3(1, 1, 1));
	mat4 getPlacement();
	vec3 getPosition();
	vec3 getRotation();
	vec3 getScale();
	Model* getModel();

protected:
	// Model properties
	const std::string c_modelPath = "../../Models/";
	const std::string c_texturePath = "../../Textures/";
	std::string m_name;
	std::vector<TextureID> textures;
	std::vector<TextureID> bumpMaps;
	std::vector<int> tile;

	vec3 m_InitPosition, m_InitRotation, m_InitSize;

	mat4 m_placement; // position (translation) & rotation (World matrix)
	ShaderID *p_shader;
	SamplerStateID *p_filter;
	Renderer *p_renderer;
	Model *p_model;
};

#endif // _STATICMODEL_H_