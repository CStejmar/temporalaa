//_________________________________________________________//
// File: SubpixelBuffer.cpp								   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: SubpixelBuffer class functions			   //
//_________________________________________________________//

#include "SubpixelBuffer.h"

SubpixelBuffer::SubpixelBuffer(Renderer* renderer, mat4 projectionMatrix, int width, int height)
{
	if ((m_SubpixelRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}
	m_ProjectionMatrix = projectionMatrix;
	m_ViewMatrix = identity4();
}

SubpixelBuffer::~SubpixelBuffer()
{

}

void SubpixelBuffer::setViewMatrix(mat4 viewMatrix)
{
	m_ViewMatrix = viewMatrix;
}

mat4 SubpixelBuffer::getViewMatrix()
{
	return m_ViewMatrix;
}

mat4 SubpixelBuffer::getViewProjectionMatrix()
{
	return m_ProjectionMatrix*m_ViewMatrix;
}

void SubpixelBuffer::resetBuffer(Direct3D11Renderer *d3D11_renderer)
{
	d3D11_renderer->clearRenderTarget(m_SubpixelRT, float4(0.0f, 0.0f, 0.0f, 0.0f));
	m_ViewMatrix = identity4();
}
