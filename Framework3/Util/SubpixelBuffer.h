//_________________________________________________________//
// File: SubpixelBuffer.h								   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: SubpixelBuffer class declaration		   //
//_________________________________________________________//

#ifndef _SUBPIXELBUFFER_H_
#define _SUBPIXELBUFFER_H_

#include "../Renderer.h"
#include "../Direct3D11/D3D11App.h"

#include <string>
#include <vector>

class SubpixelBuffer {

public:
	TextureID m_SubpixelRT;

	// Constructor
	SubpixelBuffer(Renderer* renderer, mat4 projectionMatrix, int width, int height);
	// Destructor
	~SubpixelBuffer();

	// Functions
	void setViewMatrix(mat4 viewMatrix);
	mat4 getViewMatrix();
	mat4 getViewProjectionMatrix();

	void resetBuffer(Direct3D11Renderer *d3D11_renderer);

private:
	mat4 m_ProjectionMatrix, m_ViewMatrix;
};

#endif // _SUBPIXELBUFFER_H_