//_________________________________________________________//
// File: T2MSAA.cpp										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: T2MSAA class functions					   //
//_________________________________________________________//

#include "T2MSAA.h"

T2MSAA::T2MSAA(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane, mat4 projectionMatrix)
{
	p_renderer = renderer;
	m_LinearDepthParams = linearDepthParams;
	m_InvFar = float4(1.0f / farPlane, 0.0f, 0.0f, 0.0f);

	m_ProjectionMatrix = projectionMatrix;
	m_ViewMatrix = identity4();
	m_PrevViewMatrix = identity4();
	m_PrevProjectionMatrix = identity4();

	if ((m_shader = renderer->addShader(c_shaderPath.c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_shaderPath.c_str());

		ErrorMsg(str);
	}

	// Final frame render target
	if ((m_FrameRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	// Number of samples per pixel render target
	if ((m_SamplesRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_R16UI, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	if ((m_TempSamplesRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_R16UI, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	// Bilinear sampler for depth test of previous frames
	if ((m_Filter = renderer->addSamplerState(BILINEAR, CLAMP, CLAMP, CLAMP)) == SS_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add sampler state");

		ErrorMsg(str);
	}
}

T2MSAA::~T2MSAA()
{

}

void T2MSAA::resize(const int w, const int h)
{
	p_renderer->resizeRenderTarget(m_FrameRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_SamplesRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_TempSamplesRT, w, h, 1, 1, 1);
}

TextureID T2MSAA::getRT()
{
	return m_FrameRT;
}

void T2MSAA::setViewMatrix(mat4 matrix)
{
	m_ViewMatrix = matrix;
}

void T2MSAA::setPrevViewMatrix(mat4 matrix)
{
	m_PrevViewMatrix = matrix;
}

void T2MSAA::setProjectionMatrix(mat4 matrix)
{
	m_ProjectionMatrix = matrix;
}

void T2MSAA::setPrevProjectionMatrix(mat4 matrix)
{
	m_PrevProjectionMatrix = matrix;
}

void T2MSAA::reset()
{
	Direct3D11Renderer *d3D11_renderer = ((Direct3D11Renderer *)p_renderer);
	d3D11_renderer->clearRenderTarget(m_SamplesRT, float4(0.0f, 0.0f, 0.0f, 0.0f));
	//m_FrameRT, m_TempSamplesRT does not have to be reset because they are always overwritten
}

// Gaussian random number by Marsaglia polar method (Version of Box�Muller transform)
float2 T2MSAA::getGaussianOffset(int seed)
{
	double mean = 0.0;
	double variance = 0.2575*0.2575; // standard deviation from "Amortized supersampling"

	srand(seed);

	double u = 0.0;
	double v = 0.0;
	double s = 0.0;

	do
	{
		u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
		v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
		s = u * u + v * v;
	} while ((s >= 1.0) || (s == 0.0));

	s = sqrt(-2.0 * log(s) / s);

	return float2((float)(mean + variance * u * s), (float)(mean + variance * v * s));
}

void T2MSAA::apply(TextureID currentFrameMS, TextureID previousFrame, TextureID depthMS, TextureID prevDepthMS, TextureID velocityRT, ID3D11DeviceContext* context, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID pointClamp, int msaaSamples, int width, int height)
{
	TextureID RTs[] = { m_FrameRT, m_TempSamplesRT };
	p_renderer->changeRenderTargets(RTs, elementsOf(RTs), TEXTURE_NONE);

	p_renderer->reset();
	p_renderer->setDepthState(depthState);

	p_renderer->setShader(m_shader);

	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setSamplerState("BilinearSampler", m_Filter);
	p_renderer->setSamplerState("PointSampler", pointClamp);

	p_renderer->setTexture("CurrentFrameMS", currentFrameMS);
	p_renderer->setTexture("PreviousFrame", previousFrame);
	p_renderer->setTexture("Velocity", velocityRT);
	p_renderer->setTexture("DepthTexture", depthMS);
	p_renderer->setTexture("PrevDepthTexture", prevDepthMS);
	p_renderer->setTexture("SamplesTexture", m_SamplesRT);

	p_renderer->setShaderConstant2f("LinearDepthParams", m_LinearDepthParams);
	p_renderer->setShaderConstant2f("TextureSize", float2((float)width, (float)height));
	p_renderer->setShaderConstant1i("MSAASamples", msaaSamples);

	p_renderer->apply();

	p_renderer->drawArrays(PRIM_TRIANGLES, 0, 3);

	context->CopyResource(((Direct3D11Renderer*)p_renderer)->getResource(m_SamplesRT), ((Direct3D11Renderer*)p_renderer)->getResource(m_TempSamplesRT));
}