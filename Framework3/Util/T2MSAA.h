//_________________________________________________________//
// File: T2MSAA.h										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: T2MSAA class declaration				   //
//_________________________________________________________//

#ifndef _T2MSAA_H_
#define _T2MSAA_H_

#include "../Renderer.h"
#include "../Direct3D11/Direct3D11Renderer.h"

#include <string>
#include <vector>

class T2MSAA {

public:
	// Constructor and destructor
	T2MSAA(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane, mat4 projectionMatrix);
	~T2MSAA();

	// Functions
	void resize(const int w, const int h);
	TextureID getRT();
	void setViewMatrix(mat4 matrix);
	void setPrevViewMatrix(mat4 matrix);
	void setProjectionMatrix(mat4 matrix);
	void setPrevProjectionMatrix(mat4 matrix);
	void reset();
	float2 getGaussianOffset(int seed);

	void apply(TextureID currentFrameMS, TextureID previousFrame, TextureID depthMS, TextureID prevDepthMS, TextureID velocityRT, ID3D11DeviceContext* context, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID pointClamp, int msaaSamples, int width, int height);

private:

	TextureID m_FrameRT, m_SamplesRT, m_TempSamplesRT;

	mat4 m_ProjectionMatrix, m_ViewMatrix, m_PrevProjectionMatrix, m_PrevViewMatrix;

	float2 m_LinearDepthParams;
	float4 m_InvFar;

	ShaderID m_shader;
	SamplerStateID m_Filter;

	const std::string c_shaderPath = "../../Shaders/T2MSAA.shd";

	Renderer* p_renderer;
};

#endif // _T2MSAA_H_