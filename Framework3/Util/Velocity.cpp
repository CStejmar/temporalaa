//_________________________________________________________//
// File: Velocity.cpp									   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: Class functions for velocity calculations  //	
//_________________________________________________________//

#include "Velocity.h"

Velocity::Velocity(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane)
{
	p_renderer = renderer;
	m_LinearDepthParams = linearDepthParams;
	m_InvFar = float4(1.0f / farPlane, 0.0f, 0.0f, 0.0f);

	if ((m_shader1 = renderer->addShader((c_VelocityBufferShaderPath).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_VelocityBufferShaderPath.c_str());

		ErrorMsg(str);
	}
	if ((m_shader2 = renderer->addShader((c_VelocityShaderPath).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_VelocityShaderPath.c_str());

		ErrorMsg(str);
	}
	if ((m_shader2MS = renderer->addShader((c_VelocityShaderMSPath).c_str())) == SHADER_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't load shader: %s", c_VelocityShaderMSPath.c_str());

		ErrorMsg(str);
	}

	// If it wasn't for debugg purposes the format could be changed to RGB16
	if ((m_VelocityBufferRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA16F, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	// If it wasn't for debugg purposes the format could be changed to RG16
	if ((m_HPVelocityRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA16F, 1, SS_NONE)) == TEXTURE_NONE)
	{
		char str[256];
		sprintf(str, "Couldn't add texture");

		ErrorMsg(str);
	}

	m_ViewProjMatrix = identity4();
	m_PrevViewProjMatrix = identity4();
	m_WorldViewProjMatrix = identity4();
	m_PrevWorldViewProjMatrix = identity4();
}

Velocity::~Velocity()
{

}

void Velocity::resize(const int w, const int h)
{
	p_renderer->resizeRenderTarget(m_VelocityBufferRT, w, h, 1, 1, 1);
	p_renderer->resizeRenderTarget(m_HPVelocityRT, w, h, 1, 1, 1);
}

void Velocity::setViewProjMatrix(mat4 matrix)
{
	m_ViewProjMatrix = matrix;
}

void Velocity::setPrevViewProjMatrix(mat4 matrix)
{
	m_PrevViewProjMatrix = matrix;
}

void Velocity::setWorldViewProjMatrix(mat4 worldMatrix)
{
	m_WorldViewProjMatrix = m_ViewProjMatrix*worldMatrix;
}

void Velocity::setPrevWorldViewProjMatrix(mat4 prevWorldMatrix)
{
	m_PrevWorldViewProjMatrix = m_PrevViewProjMatrix*prevWorldMatrix;
}

TextureID Velocity::getVelocityBuffer()
{
	return m_VelocityBufferRT;
}

TextureID Velocity::getVelocityTexture()
{
	return m_HPVelocityRT;
}

void Velocity::calculateVelocityBuffer(DynamicModel* dynamicModel, DepthStateID depthState, RasterizerStateID rasterizerState)
{
	mat4 currentWorldMat = dynamicModel->getPlacement();
	mat4 previousWorldMat = dynamicModel->getPrevPlacement();

	setWorldViewProjMatrix(currentWorldMat);
	setPrevWorldViewProjMatrix(previousWorldMat);

	p_renderer->reset();
	p_renderer->setDepthState(depthState);
	p_renderer->setShader(m_shader1);
	p_renderer->setRasterizerState(rasterizerState);

	p_renderer->setShaderConstant4x4f("WorldViewProjection", m_WorldViewProjMatrix);
	p_renderer->setShaderConstant4x4f("PreviousWorldViewProjection", m_PrevWorldViewProjMatrix);

	p_renderer->apply();
	dynamicModel->getModel()->draw(p_renderer);
}

void Velocity::calculateVelocityTexture(TextureID depthRT, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID pointClamp, float3 camPos, bool multiSampling, int msaaSamples)
{
	p_renderer->reset();
	p_renderer->setDepthState(depthState);

	if (multiSampling)
		p_renderer->setShader(m_shader2MS);
	else
		p_renderer->setShader(m_shader2);

	p_renderer->setRasterizerState(rasterizerState);

	mat4 InvViewProjMatrix = m_ViewProjMatrix;
	InvViewProjMatrix.translate(camPos);  // Removes translation. This is then added again in shader as EyePosition

	InvViewProjMatrix = !InvViewProjMatrix;

	p_renderer->setShaderConstant4x4f("ViewProjInv", InvViewProjMatrix);
	p_renderer->setShaderConstant4f("InvFar", m_InvFar);

	p_renderer->setTexture("VelocityTexture", m_VelocityBufferRT);
	p_renderer->setTexture("DepthTexture", depthRT);
	p_renderer->setSamplerState("PointSampler", pointClamp);

	p_renderer->setShaderConstant4x4f("PrevViewProjMatrix", m_PrevViewProjMatrix);
	p_renderer->setShaderConstant3f("EyePosition", camPos);
	p_renderer->setShaderConstant2f("LinearDepthParams", m_LinearDepthParams);

	if (multiSampling)
		p_renderer->setShaderConstant1i("MSAASamples", msaaSamples);

	p_renderer->apply();
	p_renderer->drawArrays(PRIM_TRIANGLES, 0, 3);
}