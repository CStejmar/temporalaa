//_________________________________________________________//
// File: Velocity.h										   //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: Class for velocity calculations			   //	
//_________________________________________________________//

#ifndef _VELOCITY_H_
#define _VELOCITY_H_

#include "../Direct3D11/Direct3D11Renderer.h"
#include "../Renderer.h"
#include "DynamicModel.h"

#include <string>

class Velocity {

public:
	// Constructor and destructor
	Velocity(Renderer* renderer, int width, int height, float2 linearDepthParams, float farPlane);
	~Velocity();

	// Functions
	void resize(const int w, const int h);
	void setViewProjMatrix(mat4 matrix);
	void setPrevViewProjMatrix(mat4 matrix);
	TextureID getVelocityBuffer();
	TextureID getVelocityTexture();
	
	void calculateVelocityBuffer(DynamicModel* dynamicModel, DepthStateID depthState, RasterizerStateID rasterizerState);
	void calculateVelocityTexture(TextureID depthRT, DepthStateID depthState, RasterizerStateID rasterizerState, SamplerStateID pointClamp, float3 camPos, bool multiSampling, int msaaSamples);

private:
	void setWorldViewProjMatrix(mat4 worldMatrix);
	void setPrevWorldViewProjMatrix(mat4 prevWorldMatrix);

	mat4 m_ViewProjMatrix, m_PrevViewProjMatrix;
	mat4 m_WorldViewProjMatrix, m_PrevWorldViewProjMatrix;

	float2 m_LinearDepthParams;
	float4 m_InvFar;

	TextureID m_VelocityBufferRT, m_HPVelocityRT;

	ShaderID m_shader1, m_shader2, m_shader2MS;
	
	const std::string c_VelocityBufferShaderPath = "../../Shaders/VelocityBuffer.shd";
	const std::string c_VelocityShaderPath = "../../Shaders/Velocity.shd";
	const std::string c_VelocityShaderMSPath = "../../Shaders/VelocityMS.shd";

	Renderer* p_renderer;
};

#endif // _VELOCITY_H_