//_________________________________________________________//
// File: ASAA.shd									       //
// Language: HLSL										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: ASAA shader								   //
//_________________________________________________________//

[Vertex shader]

struct VS_IN
{
	uint VertexID : SV_VertexID;
};

struct VS_OUT
{
	float4 Pos      : SV_Position;
	float2 TexCoord : TEXCOORD0;
	float3 Dir      : TEXCOORD1;
};


cbuffer FrameConstantsVS : register(b0)
{
	float4x4	ViewProjInv;
	float4		InvFar;
};

VS_OUT main(VS_IN In)
{
	VS_OUT Out;

	// Produce a fullscreen triangle
	Out.Pos.x = (In.VertexID == 0) ? 3.0f : -1.0f;
	Out.Pos.y = (In.VertexID == 2) ? 3.0f : -1.0f;
	Out.Pos.z = 0.0f;
	Out.Pos.w = 1.0f;
	Out.TexCoord = (Out.Pos.xy * float2(0.5, -0.5) + 0.5);

	float4 cDir = mul(ViewProjInv, float4(Out.Pos.xy, 0, Out.Pos.w)); // Changed order due to differences in the frameworks
	Out.Dir = (cDir.xyz / cDir.w) * InvFar.x;

	return Out;
}

[Fragment shader]

struct PS_IN
{
	float4 HPosition : SV_Position;
	float2 TexCoord  : TEXCOORD0;
	float3 Dir       : TEXCOORD1;
};

struct PsOut
{
	float4 Color          : SV_Target0;
	float4 SubpixelBuffer : SV_Target1;
	uint  Samples         : SV_Target2;
};

SamplerState BilinearSampler : register(s0);

Texture2D CurrentFrame    : register(t0);
Texture2D SubpixelBuffer0 : register(t1);
Texture2D SubpixelBuffer1 : register(t2);
Texture2D SubpixelBuffer2 : register(t3);
Texture2D SubpixelBuffer3 : register(t4);
Texture2D DepthTexture    : register(t5);
Texture2D<unsigned int> SamplesTexture  : register(t6);
Texture2D PrevDepthTexture : register(t7);


cbuffer FrameConstantsPS : register(b0)
{
	float4x4 SubpixelViewProjMatrix[5]; // The last one is the current
	float4x4 SubpixelViewProjOffsetMatrix[5]; // These matrices have a quadrant offset corresponding to the current index
	float3 EyePosition;
	float2 LinearDepthParams;
	int Index;
	float2 TextureSize;
};

#include "ASAAShaderIncludes.h"

PsOut main(PS_IN In)
{
	PsOut Out;
	Texture2D SubpixelBuffer[4] = { SubpixelBuffer0, SubpixelBuffer1, SubpixelBuffer2, SubpixelBuffer3 };

	// Retrieve depth value for the pixel.
	int3 screen_coord = int3(In.HPosition.xy, 0);
	float z_over_w = DepthTexture.Load(screen_coord).r;
	float lin_depth = LinearizeDepth(z_over_w, LinearDepthParams);

	// Calculate screen position. UV coordinates
	float4 currentPositionUV = float4(In.TexCoord.xy, z_over_w, 1.0f);

	// No need to supersample the skybox (high resolution) so perform ASAA only on objects closer than far = 2000.0f
	/*if (lin_depth < 1999.0f)
	{*/
	// Calculate world position. World coordinates
	float4 worldPosition = float4(EyePosition + (lin_depth * In.Dir), 1.0f);

	uint samples = SamplesTexture.Load(screen_coord);

	Output result = updateASAA(CurrentFrame, SubpixelBuffer, PrevDepthTexture, SubpixelViewProjMatrix, SubpixelViewProjOffsetMatrix, worldPosition, BilinearSampler, Index, samples, TextureSize, currentPositionUV.xy, LinearDepthParams);
	Out.Color = result.frame;
	Out.SubpixelBuffer = float4(result.buffer.rgb, z_over_w); // Test to store previous depth in subpixel buffers alpha channel. Result: Didn't work well, probably due to low precision
	Out.Samples = result.N;
	/*}
	else
	{
		float3 currentSample = CurrentFrame.Sample(BilinearSampler, currentPositionUV.xy, 0).rgb;
		
		Out.Color = float4(currentSample, 1.0f);
		Out.SubpixelBuffer = float4(currentSample, 1.0f);
		Out.Samples = 1;
	}*/

	return Out;
}
