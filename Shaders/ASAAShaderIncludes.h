//_________________________________________________________//
// File: ASAAShaderIncludes.h						       //
// Language: HLSL										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-27								   //
// Description: Shader functions for the different versions//
//				of ASAA									   //
//_________________________________________________________//

float LinearizeDepth(float z, float2 nfConst)
{
	return 1.f / (z * nfConst.x + nfConst.y);
}

float4 UVPosition(float4 homogeneousPos)
{
	// float4(x,y,z,1)
	homogeneousPos /= homogeneousPos.w;

	// -> [0, 1]
	homogeneousPos.xy *= float2(0.5f, -0.5f);
	homogeneousPos.xy += float2(0.5f, 0.5f);

	return homogeneousPos;
}

float updateSmoothingFactor(uint samples) // alpha = smoothingfactor
{
	float a = 1.0f / (float)(samples + 1);
	return a;
}

uint updateEffectiveSamples(uint oldSample, float alpha)
{
	// oldSample = effective number of samples at previous frame
	// alpha = updated smoothing factor
	return (uint)(1.0f / (alpha*alpha + (1.0f - alpha)*(1.0f - alpha) / (float)oldSample));
}

//float2x2 jacobian(float2 pos)
//{
//	float2x2 J = { ddx(pos.x), ddy(pos.x),
//		ddx(pos.y), ddy(pos.y) };
//	// Or maybe: float2x2 J = {ddx(pos), ddy(pos)}';
//
//	return J;
//}
//
//float tentRadius(float2 pos)
//{
//	float2x2 J = jacobian(pos);
//	float2 temp = J*float2(0.5f, 0.5f);
//	float r = max(abs(temp.x), abs(temp.y));
//	return r;
//}

// Simplification of the two functions above
float2 tentRadius(float2 pos)
{
	float2 r = max(abs(ddx(pos)), abs(ddy(pos)))*0.5f;
	return r;
}

float tent(float2 radius, float2 v) // Note that v is not velocity! v1 = vector1, v2 = vector2, v = v2 - v1
{
	return clamp(1 - abs(v.x) / radius.x, 0, 1)*clamp(1 - abs(v.y) / radius.y, 0, 1);
}

float4x4 translateMatrix(float4x4 m, float3 translation)
{
	m[0][3] += dot(m[0].xyz, translation);
	m[1][3] += dot(m[1].xyz, translation);
	m[2][3] += dot(m[2].xyz, translation);
	m[3][3] += dot(m[3].xyz, translation);

	return m; // translatedMatrix;
}

struct Output
{
	float4 buffer;
	float4 frame;
	uint N; //Number of samples
};

struct OutputExp
{
	float4 buffer;
	float4 frame;
	uint4 N; //Number of samples
};

// Used with class ASAA. This function uses the resampling proposed in Amortized Supersampling, a version of bilinear resampling (hardware implementation).
Output updateASAA(Texture2D currentFrame, Texture2D subpixelBuffer[4], Texture2D prevDepthTexture, float4x4 subpixelViewProjMatrix[5], float4x4 subpixelViewProjOffsetMatrix[5], float4 worldPosition, SamplerState bilinearSampler, int index, uint samples, float2 screenSize, float2 currentPositionUV, float2 linearDepthParams)
{
	float K = 4.0f;
	float2 quadrant[4] = { float2(-0.25f, -0.25f), float2(-0.25f, 0.25f), float2(0.25f, -0.25f), float2(0.25f, 0.25f) };
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float3 b = float3(0.0f, 0.0f, 0.0f);
	float3 f = float3(0.0f, 0.0f, 0.0f);

	int index_prev[4];

	if ((index - 1) == -1)
		index_prev[0] = 3;
	else
		index_prev[0] = index - 1;

	if ((index - 2) == -1)
		index_prev[1] = 3;
	else if ((index - 2) == -2)
		index_prev[1] = 2;
	else
		index_prev[1] = index - 2;

	if ((index - 3) == -1)
		index_prev[2] = 3;
	else if ((index - 3) == -2)
		index_prev[2] = 2;
	else if ((index - 3) == -3)
		index_prev[2] = 1;
	else
		index_prev[2] = index - 3;

	if ((index - 4) == -1)
		index_prev[3] = 3;
	else if ((index - 4) == -2)
		index_prev[3] = 2;
	else if ((index - 4) == -3)
		index_prev[3] = 1;
	else if ((index - 4) == -4)
		index_prev[3] = 0;

	//float2 quadTest = float2(0.0f, 0.0f);

	float w_b = 0.0f;
	float w_f = 0.0f;

	int sampleUpdate = samples;

	float2 r = 0.0f;

	for (uint k = 0; k < 4; k++)
	{
		float4 previousPositionUV = mul(subpixelViewProjMatrix[k], worldPosition);
		previousPositionUV = previousPositionUV / previousPositionUV.w; // I.e. Normalized Device Coordinates (NDC)

		// Map position to [0,1]
		previousPositionUV.xy *= float2(0.5f, -0.5f); // UV coordinates
		previousPositionUV.xy += float2(0.5f, 0.5f); // UV coordinates

		float2 previousPositionSS = previousPositionUV.xy*screenSize; // Screen Space coordinates
		float2 iPreviousPositionSS = floor(previousPositionSS); // Integer coordinate

		if (abs(previousPositionUV.x - currentPositionUV.x) != 0.0f || abs(previousPositionUV.y - currentPositionUV.y) != 0.0f)
		sampleUpdate = 4;

		//quadTest = previousPositionSS - iPreviousPositionSS; //previousPositionOffsetSS - iPreviousPositionOffsetSS;

		float pDepth[4] = { prevDepthTexture.Sample(bilinearSampler, previousPositionUV.xy, 0).r, prevDepthTexture.Sample(bilinearSampler, previousPositionUV.xy, 0).g, prevDepthTexture.Sample(bilinearSampler, previousPositionUV.xy, 0).b, prevDepthTexture.Sample(bilinearSampler, previousPositionUV.xy, 0).a };

		float prevLinDepth = LinearizeDepth(pDepth[k], linearDepthParams);
		float currLinDepth = LinearizeDepth(previousPositionUV.z, linearDepthParams);

		bool miss = false;

		float2 pixelRadius = max(ddx(previousPositionSS), ddy(previousPositionSS));

		// Miss if the reprojected (previous) position is more than pixelRadius away from closest pixel (integer value)
		miss = miss || abs(previousPositionSS.x - round(previousPositionSS.x)) > pixelRadius.x || abs(previousPositionSS.y - round(previousPositionSS.y)) > pixelRadius.y;

		// Miss if the depth difference is over a dynamic and linear threshold
		miss = miss || (currLinDepth - prevLinDepth) > (0.08f*currLinDepth);

		// Miss if the previous position is outside of the frustum(screen)
		miss = miss || saturate(previousPositionUV.x) != previousPositionUV.x || saturate(previousPositionUV.y) != previousPositionUV.y;// || saturate(previousPositionOffsetUV.x) != previousPositionOffsetUV.x || saturate(previousPositionOffsetUV.y) != previousPositionOffsetUV.y;


		if (!miss)
		{
			float4 previousPositionOffsetUV = mul(subpixelViewProjOffsetMatrix[k], worldPosition);
			previousPositionOffsetUV = previousPositionOffsetUV / previousPositionOffsetUV.w;

			// Map position to [0,1]
			previousPositionOffsetUV.xy *= float2(0.5f, -0.5f); // UV coordinates
			previousPositionOffsetUV.xy += float2(0.5f, 0.5f); // UV coordinates

			float2 previousPositionOffsetSS = previousPositionOffsetUV.xy*screenSize; // Screen Space coordinates
			float2 iPreviousPositionOffsetSS = floor(previousPositionOffsetSS); // Integer coordinate

			float2 p_k_b = previousPositionOffsetSS - quadrant[index_prev[k]];
			float2 iP_k_b = floor(p_k_b) + float2(0.5f, 0.5f);

			float2 p_k_f = previousPositionSS - quadrant[index_prev[k]];
			float2 iP_k_f = floor(p_k_f) + float2(0.5f, 0.5f);

			float2 radius_b = tentRadius(previousPositionSS);
			float2 radius_f = 2 * radius_b;

			r = radius_b;

			float w_k_b = 0.0f;
			float w_k_f = 0.0f;
			for (uint d = 0; d < 4; d++)
			{
				float w_temp_b = tent(radius_b, p_k_b - (iP_k_b + delta[d]));
				w_k_b += w_temp_b;
				w_b += w_temp_b;

				float w_temp_f = tent(radius_f, p_k_f - (iP_k_f + delta[d]));
				w_k_f += w_temp_f;
				w_f += w_temp_f;
			}

			float w_k01_b = tent(radius_b, p_k_b - (iP_k_b + delta[2]));
			float w_k11_b = tent(radius_b, p_k_b - (iP_k_b + delta[3]));
			float w_k10_b = tent(radius_b, p_k_b - (iP_k_b + delta[1]));
			float2 offset_b = float2((w_k01_b + w_k11_b), (w_k10_b + w_k11_b)) / w_k_b;

			float w_k01_f = tent(radius_f, p_k_f - (iP_k_f + delta[2]));
			float w_k11_f = tent(radius_f, p_k_f - (iP_k_f + delta[3]));
			float w_k10_f = tent(radius_f, p_k_f - (iP_k_f + delta[1]));
			float2 offset_f = float2((w_k01_f + w_k11_f), (w_k10_f + w_k11_f)) / w_k_f;

			float2 sample_pos_b = iP_k_b + offset_b;
			float2 sample_pos_f = iP_k_f + offset_f;

			b += w_k_b*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_b) / screenSize, 0, 1), 0).rgb;
			f += w_k_f*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_f) / screenSize, 0, 1), 0).rgb;

			sampleUpdate += 1;
		}
		else
			sampleUpdate = 0;
	}

	if (w_b != 0.0f)
		b = b / w_b;
	if (w_f != 0.0f)
		f = f / w_f;

	// Get the new sample, i.e. the current sample
	float3 currentSample = currentFrame[uint2(currentPositionUV*screenSize)].rgb; // Equivalent to point sampling

	float alpha = updateSmoothingFactor(sampleUpdate);

	Output result;

	float4 buffer = float4(alpha*currentSample, 1.0f) + float4((1 - alpha)*b, 1.0f);

	float4 frame;
	if (alpha == 1.0f)
		frame = float4(currentSample, 1.0f);
	else
		frame = float4((alpha / K)*currentSample, 1.0f) + float4((1 - alpha / K)*f, 1.0f);

	// Check radius for bilinear resampling
	//frame = float4(r.x, r.y, 0.0f, 1.0f);

	//Test subpixel quadrant offsets
	/*if (quadTest.x < 0.5f && quadTest.y < 0.5f)
	frame = float4(1.0f, 0.0f, 0.0f, 1.0f);
	if (quadTest.x < 0.5f && quadTest.y > 0.5f)
	frame = float4(0.0f, 1.0f, 0.0f, 1.0f);
	if (quadTest.x > 0.5f && quadTest.y < 0.5f)
	frame = float4(0.0f, 0.0f, 1.0f, 1.0f);
	if (quadTest.x > 0.5f && quadTest.y > 0.5f)
	frame = float4(1.0f, 0.0f, 1.0f, 1.0f);*/

	// Test without offsets
	/*if (quadTest.x == 0.5f && quadTest.y == 0.5f)
	frame = float4(1.0f, 0.0f, 0.0f, 1.0f);
	else if (quadTest.x < 0.55f && quadTest.x > 0.45f && quadTest.y < 0.55f && quadTest.y > 0.45f)
	frame = float4(0.0f, 1.0f, 0.0f, 1.0f);
	else
	frame = float4(0.0f, 0.0f, 1.0f, 1.0f);*/

	result.buffer = buffer;
	result.frame = frame;
	result.N = sampleUpdate;
	return result;
}

// Used with class ASAA. This function uses bilinear resampling, but not the hardware implementation.
Output updateASAA2(Texture2D currentFrame, Texture2D subpixelBuffer[4], Texture2D prevDepthTexture, float4x4 subpixelViewProjMatrix[5], float4x4 subpixelViewProjOffsetMatrix[5], float4 worldPosition, SamplerState bilinearSampler, int index, uint samples, float2 screenSize, float2 currentPositionUV, float2 linearDepthParams)
{
	float K = 4.0f;
	float2 quadrant[4] = { float2(-0.25f, -0.25f), float2(-0.25f, 0.25f), float2(0.25f, -0.25f), float2(0.25f, 0.25f) };
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float3 b = float3(0.0f, 0.0f, 0.0f);
	float3 f = float3(0.0f, 0.0f, 0.0f);

	int index_prev[4];

	if ((index - 1) == -1)
		index_prev[0] = 3;
	else
		index_prev[0] = index - 1;

	if ((index - 2) == -1)
		index_prev[1] = 3;
	else if ((index - 2) == -2)
		index_prev[1] = 2;
	else
		index_prev[1] = index - 2;

	if ((index - 3) == -1)
		index_prev[2] = 3;
	else if ((index - 3) == -2)
		index_prev[2] = 2;
	else if ((index - 3) == -3)
		index_prev[2] = 1;
	else
		index_prev[2] = index - 3;

	if ((index - 4) == -1)
		index_prev[3] = 3;
	else if ((index - 4) == -2)
		index_prev[3] = 2;
	else if ((index - 4) == -3)
		index_prev[3] = 1;
	else if ((index - 4) == -4)
		index_prev[3] = 0;

	/*uint samples_b = samples;
	uint samples_f = samples;*/
	int sampleUpdate = samples;

	float w_b = 0.0f;
	float w_f = 0.0f;

	for (uint k = 0; k < 4; k++)
	{
		float4 previousPositionUV = mul(subpixelViewProjMatrix[k], worldPosition);
		previousPositionUV = previousPositionUV / previousPositionUV.w;

		// Map position to [0,1]
		previousPositionUV.xy *= float2(0.5f, -0.5f); // UV coordinates
		previousPositionUV.xy += float2(0.5f, 0.5f); // UV coordinates

		if (!(saturate(previousPositionUV.x) != previousPositionUV.x || saturate(previousPositionUV.y) != previousPositionUV.y))
		{
			float2 previousPositionSS = previousPositionUV.xy*screenSize; // Screen Space coordinates
			float2 iPreviousPositionSS = floor(previousPositionSS); // Integer coordinate

			float4 previousPositionOffsetUV = mul(subpixelViewProjOffsetMatrix[k], worldPosition);
			previousPositionOffsetUV = previousPositionOffsetUV / previousPositionOffsetUV.w;

			previousPositionOffsetUV.xy *= float2(0.5f, -0.5f); // UV coordinates
			previousPositionOffsetUV.xy += float2(0.5f, 0.5f); // UV coordinates

			float2 previousPositionOffsetSS = previousPositionOffsetUV.xy*screenSize; // Screen Space coordinates
			float2 iPreviousPositionOffsetSS = floor(previousPositionOffsetSS); // Integer coordinate

			float2 p_k_f = previousPositionSS;
			float2 p_k_b = previousPositionOffsetSS;

			float radius_b = tentRadius(previousPositionSS);
			float radius_f = 2 * radius_b;

			for (uint d = 0; d < 4; d++)
			{
				float w_k_b = tent(radius_b, previousPositionOffsetSS - (iPreviousPositionOffsetSS + 0.5f + quadrant[index] - quadrant[index_prev[k]] + delta[d]));
				w_b += w_k_b;

				float w_k_f = tent(radius_f, previousPositionSS - (iPreviousPositionSS + 0.5f - quadrant[index_prev[k]] + delta[d]));
				w_f += w_k_f;

				b += w_k_b*subpixelBuffer[k][uint2(iPreviousPositionOffsetSS + delta[d])].rgb;
				f += w_k_f*subpixelBuffer[k][uint2(iPreviousPositionSS + delta[d])].rgb;
			}
			sampleUpdate += 1;
		}
		else
		{
			sampleUpdate = 0;
		}
	}

	if (w_b != 0.0f)
		b = b / w_b;
	if (w_f != 0.0f)
		f = f / w_f;

	// Get the new sample, i.e. the current sample
	float3 currentSample = currentFrame[uint2(currentPositionUV*screenSize)].rgb; // Equivalent to point sampling

	float alpha = updateSmoothingFactor(sampleUpdate);
	alpha = max(alpha, 0.0001f);

	Output result;

	float4 buffer = float4(alpha*currentSample, 1.0f) + float4((1 - alpha)*b, 1.0f);
	float4 frame;
	if (alpha == 1.0f)
		frame = float4(currentSample, 1.0f);
	else
		frame = float4((alpha / K)*currentSample, 1.0f) + float4((1 - alpha / K)*f, 1.0f);

	result.buffer = buffer;
	result.frame = frame;
	result.N = sampleUpdate;
	return result;
}

// Used with the class ASAA and the function applyWVP. Used when calculating new positions with WVP matrices for all models.
Output updateASAAwvp(float4 positions[4], float4 subpixelPositions[5], Texture2D currentFrame, Texture2D subpixelBuffer[4], SamplerState bilinearSampler, int index, uint samples, float2 screenSize, float alpha)
{
	float K = 4.0f;
	float2 quadrant[4] = { float2(-0.25f, -0.25f), float2(-0.25f, 0.25f), float2(0.25f, -0.25f), float2(0.25f, 0.25f) };
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float3 b = float3(0.0f, 0.0f, 0.0f);
	float3 f = float3(0.0f, 0.0f, 0.0f);

	int index_prev[4];

	if ((index - 1) == -1)
		index_prev[0] = 3;
	else
		index_prev[0] = index - 1;

	if ((index - 2) == -1)
		index_prev[1] = 3;
	else if ((index - 2) == -2)
		index_prev[1] = 2;
	else
		index_prev[1] = index - 2;

	if ((index - 3) == -1)
		index_prev[2] = 3;
	else if ((index - 3) == -2)
		index_prev[2] = 2;
	else if ((index - 3) == -3)
		index_prev[2] = 1;
	else
		index_prev[2] = index - 3;

	if ((index - 4) == -1)
		index_prev[3] = 3;
	else if ((index - 4) == -2)
		index_prev[3] = 2;
	else if ((index - 4) == -3)
		index_prev[3] = 1;
	else if ((index - 4) == -4)
		index_prev[3] = 0;

	float2 quadTest = float2(0.0f, 0.0f);

	float w_b = 0.0f;
	float w_f = 0.0f;

	int sampleUpdate = samples;

	float2 r = 0.0f;

	for (uint k = 0; k < 4; k++)
	{
		float4 previousPositionUV = positions[k];

		float2 previousPositionSS = previousPositionUV.xy*screenSize; // Screen Space coordinates
		float2 iPreviousPositionSS = floor(previousPositionSS); // Integer coordinate

		//if (abs(previousPositionSS.x - subpixelPositions[4].x*screenSize.x) > 0.5f || abs(previousPositionSS.y - subpixelPositions[4].y*screenSize.y) > 0.5f)
		//	sampleUpdate = 0;

		float4 previousSubpixelPositionUV = subpixelPositions[k];

		float2 previousSubpixelPositionSS = previousSubpixelPositionUV.xy*screenSize; // Screen Space coordinates
		float2 iPreviousSubpixelPositionSS = floor(previousSubpixelPositionSS); // Integer coordinate

		if (saturate(previousPositionUV.x) == previousPositionUV.x && saturate(previousPositionUV.y) == previousPositionUV.y && saturate(previousSubpixelPositionUV.x) == previousSubpixelPositionUV.x && saturate(previousSubpixelPositionUV.y) == previousSubpixelPositionUV.y)
		{
			float2 p_k_b = previousSubpixelPositionSS - quadrant[index_prev[k]];
			float2 iP_k_b = floor(p_k_b) + float2(0.5f, 0.5f);
			float2 p_k_f = previousPositionSS - quadrant[index_prev[k]];
			float2 iP_k_f = floor(p_k_f) + float2(0.5f, 0.5f);

			float2 radius_b = tentRadius(previousPositionSS);
			float2 radius_f = 2 * radius_b;

			r = radius_b;

			float w_k_b = 0.0f;
			float w_k_f = 0.0f;
			for (uint d = 0; d < 4; d++)
			{
				float w_temp_b = tent(radius_b, p_k_b - (iP_k_b + delta[d]));
				w_k_b += w_temp_b;
				w_b += w_temp_b;

				float w_temp_f = tent(radius_f, p_k_f - (iP_k_f + delta[d]));
				w_k_f += w_temp_f;
				w_f += w_temp_f;
			}

			float w_k01_b = tent(radius_b, p_k_b - (iP_k_b + delta[2]));
			float w_k11_b = tent(radius_b, p_k_b - (iP_k_b + delta[3]));
			float w_k10_b = tent(radius_b, p_k_b - (iP_k_b + delta[1]));
			float2 offset_b = float2((w_k01_b + w_k11_b), (w_k10_b + w_k11_b)) / w_k_b;

			float w_k01_f = tent(radius_f, p_k_f - (iP_k_f + delta[2]));
			float w_k11_f = tent(radius_f, p_k_f - (iP_k_f + delta[3]));
			float w_k10_f = tent(radius_f, p_k_f - (iP_k_f + delta[1]));
			float2 offset_f = float2((w_k01_f + w_k11_f), (w_k10_f + w_k11_f)) / w_k_f;

			float2 sample_pos_b = iP_k_b + offset_b;
			float2 sample_pos_f = iP_k_f + offset_f;

			b += w_k_b*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_b) / screenSize, 0, 1), 0).rgb;
			f += w_k_f*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_f) / screenSize, 0, 1), 0).rgb;

			sampleUpdate += 1;
		}
		else
			sampleUpdate = 0;
	}

	if (w_b != 0.0f)
		b = b / w_b;
	if (w_f != 0.0f)
		f = f / w_f;


	float4 currentPositionOffsetUV = subpixelPositions[4];
	float3 currentSample = currentFrame.Sample(bilinearSampler, currentPositionOffsetUV.xy, 0).rgb;

	Output result;

	float4 buffer = float4(alpha*currentSample, 1.0f) + float4((1 - alpha)*b, 1.0f);
	float4 frame = float4((alpha / K)*currentSample, 1.0f) + float4((1 - alpha / K)*f, 1.0f);

	result.buffer = buffer;
	result.frame = frame;
	result.N = sampleUpdate;
	return result;
}

float blurThreshold(float alpha, float2 velocity)
{
	float sigma_g = 0.2575f;

	float variance = sigma_g*sigma_g + ((1.0f - alpha) / alpha)*((velocity.x*(1.0f - velocity.x) + velocity.y*(1.0f - velocity.y)) / 2.0f);

	return variance; // 1.0f / variance;
}

//  Used with class ASAAT. Uses exponential smoothing. Samples from older frames are weighted less
OutputExp updateASAAexp(Texture2D currentFrame, Texture2D subpixelBuffer[4], float4 worldPosition, float4x4 subpixelViewProjMatrix[5], float4x4 subpixelViewProjOffsetMatrix[5], SamplerState bilinearSampler, int index, Texture2D<uint4> samplesTexture, float2 screenSize, float2 currentPositionUV)
{
	float K = 4.0f;
	float2 quadrant[4] = { float2(-0.25f, -0.25f), float2(-0.25f, 0.25f), float2(0.25f, -0.25f), float2(0.25f, 0.25f) };
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float3 b[4] = { float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f) };
	float3 f[4] = { float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f) };

	uint samples[4];

	//float Tb = 0.0f; // Blur threshold

	int index_prev[4];

	if ((index - 1) == -1)
		index_prev[0] = 3;
	else
		index_prev[0] = index - 1;

	if ((index - 2) == -1)
		index_prev[1] = 3;
	else if ((index - 2) == -2)
		index_prev[1] = 2;
	else
		index_prev[1] = index - 2;

	if ((index - 3) == -1)
		index_prev[2] = 3;
	else if ((index - 3) == -2)
		index_prev[2] = 2;
	else if ((index - 3) == -3)
		index_prev[2] = 1;
	else
		index_prev[2] = index - 3;

	if ((index - 4) == -1)
		index_prev[3] = 3;
	else if ((index - 4) == -2)
		index_prev[3] = 2;
	else if ((index - 4) == -3)
		index_prev[3] = 1;
	else if ((index - 4) == -4)
		index_prev[3] = 0;

	//float2 quadTest = float2(0.0f, 0.0f);

	float w_b = 0.0f;
	float w_f = 0.0f;
	float2 r = 0.0f;

	float2 velocity = float2(0.0f, 0.0f);

	for (uint k = 0; k < 4; k++)
	{
		float4 previousPositionUV = mul(subpixelViewProjMatrix[k], worldPosition);
		previousPositionUV = previousPositionUV / previousPositionUV.w;

		// Map position to [0,1]
		previousPositionUV.xy *= float2(0.5f, -0.5f); // UV coordinates
		previousPositionUV.xy += float2(0.5f, 0.5f); // UV coordinates

		float2 previousPositionSS = previousPositionUV.xy*screenSize; // Screen Space coordinates
		float2 iPreviousPositionSS = floor(previousPositionSS); // Integer coordinate

		if (k == 0)
			samples[0] = samplesTexture[int2(iPreviousPositionSS)].x;
		else if (k == 1)
			samples[1] = samplesTexture[int2(iPreviousPositionSS)].y;
		else if (k == 2)
			samples[2] = samplesTexture[int2(iPreviousPositionSS)].z;
		else if (k == 3)
			samples[3] = samplesTexture[int2(iPreviousPositionSS)].w;

		//velocity = abs(previousPositionUV.xy - currentPositionUV.xy);

		//if (abs(previousPositionSS.x - currentPositionUV.x*screenSize.x) > 0.5f || abs(previousPositionSS.y - currentPositionUV.y*screenSize.y) > 0.5f)// || abs(previousPosition.z - worldPosition.z) > 1.5f)//if ((previousPositionSS.x - iPreviousPositionSS.x) > 0.5f || (previousPositionSS.y - iPreviousPositionSS.y) > 0.5f)
		//	sampleUpdate = 16;

		if (!(saturate(previousPositionUV.x) != previousPositionUV.x || saturate(previousPositionUV.y) != previousPositionUV.y))
		{
			float4 previousPositionOffsetUV = mul(subpixelViewProjOffsetMatrix[k], worldPosition);
			previousPositionOffsetUV = previousPositionOffsetUV / previousPositionOffsetUV.w;

			// Map position to [0,1]
			previousPositionOffsetUV.xy *= float2(0.5f, -0.5f); // UV coordinates
			previousPositionOffsetUV.xy += float2(0.5f, 0.5f); // UV coordinates

			float2 previousPositionOffsetSS = previousPositionOffsetUV.xy*screenSize; // Screen Space coordinates
			float2 iPreviousPositionOffsetSS = floor(previousPositionOffsetSS); // Integer coordinate

			float2 p_k_b = previousPositionOffsetSS - quadrant[index_prev[k]];
			float2 iP_k_b = floor(p_k_b) + float2(0.5f, 0.5f);
			float2 p_k_f = previousPositionSS - quadrant[index_prev[k]];
			float2 iP_k_f = floor(p_k_f) + float2(0.5f, 0.5f);

			float2 radius_b = tentRadius(previousPositionSS);

			float2 radius_f = 2 * radius_b;

			r = radius_b;

			float w_k_b = 0.0f;
			float w_k_f = 0.0f;
			for (uint d = 0; d < 4; d++)
			{
				float w_temp_b = tent(radius_b, p_k_b - (iP_k_b + delta[d]));
				w_k_b += w_temp_b;
				w_b += w_temp_b;

				float w_temp_f = tent(radius_f, p_k_f - (iP_k_f + delta[d]));
				w_k_f += w_temp_f;
				w_f += w_temp_f;
			}

			float w_k01_b = tent(radius_b, p_k_b - (iP_k_b + delta[2]));
			float w_k11_b = tent(radius_b, p_k_b - (iP_k_b + delta[3]));
			float w_k10_b = tent(radius_b, p_k_b - (iP_k_b + delta[1]));
			float2 offset_b = float2((w_k01_b + w_k11_b), (w_k10_b + w_k11_b)) / w_k_b;

			float w_k01_f = tent(radius_f, p_k_f - (iP_k_f + delta[2]));
			float w_k11_f = tent(radius_f, p_k_f - (iP_k_f + delta[3]));
			float w_k10_f = tent(radius_f, p_k_f - (iP_k_f + delta[1]));
			float2 offset_f = float2((w_k01_f + w_k11_f), (w_k10_f + w_k11_f)) / w_k_f;

			float2 sample_pos_b = iP_k_b + offset_b;
			float2 sample_pos_f = iP_k_f + offset_f;

			b[k] = w_k_b*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_b) / screenSize, 0, 1), 0).rgb;
			f[k] = w_k_f*subpixelBuffer[k].Sample(bilinearSampler, clamp((sample_pos_f) / screenSize, 0, 1), 0).rgb;
		}
		else
			samples[k] = 0;
	}

	// Get the new sample, i.e. the current sample
	float3 currentSample = currentFrame[uint2(currentPositionUV*screenSize)].rgb; // Equivalent to point sampling

	float alpha0 = updateSmoothingFactor(samples[0]);
	float alpha1 = updateSmoothingFactor(samples[1]);
	float alpha2 = updateSmoothingFactor(samples[2]);
	float alpha3 = updateSmoothingFactor(samples[3]);

	float alphaArray[4] = { alpha0, alpha1, alpha2, alpha3 };

	float s = 0.0f;
	if (alpha0 != 1.0f)
		s += 1.0f;
	if (alpha1 != 1.0f)
		s += 1.0f;
	if (alpha2 != 1.0f)
		s += 1.0f;
	if (alpha3 != 1.0f)
		s += 1.0f;

	float alpha = 0.0f;

	OutputExp result;

	float4 tempBuffer = float4(0.0f, 0.0f, 0.0f, 1.0f);
	float4 tempFrame = float4(0.0f, 0.0f, 0.0f, 1.0f);


	for (int i = 0; i < 4; i++)
	{
		/*float weightb = pow((1.0f - alphaArray[index_prev[i]]), i + 1);
		float weightf = pow((1.0f - alphaArray[index_prev[i]]), i + 1);*/

		float weight = 0.0f;

		//float diff = (1.0f - alphaArray[index_prev[i]]);

		if (i == 0)
		{
			weight = (1.0f - alphaArray[index_prev[i]]);
		}
		else if (i == 1)
		{
			weight = (1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]]);
		}
		else if (i == 2)
		{
			weight = (1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]]);
		}
		else
		{
			weight = (1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]])*(1.0f - alphaArray[index_prev[i]]);
		}

		tempBuffer.rgb += weight*b[index_prev[i]];
		tempFrame.rgb += weight*f[index_prev[i]];
		alpha += weight;
	}

	alpha = 1.0f - (alpha / s);

	float4 buffer;
	float4 frame;

	if (w_b != 0.0f)
		buffer = float4(alpha*currentSample, 1.0f) + tempBuffer / w_b;
	else
		buffer = float4(alpha*currentSample, 1.0f) + tempBuffer;

	if (w_f != 0.0f)
		frame = float4((alpha)*currentSample, 1.0f) + tempFrame / w_f;
	else
		frame = float4((alpha)*currentSample, 1.0f) + tempFrame;

	result.buffer = buffer;
	result.frame = frame;
	result.N = uint4(samples[0] + 1, samples[1] + 1, samples[2] + 1, samples[3] + 1);
	return result;
}