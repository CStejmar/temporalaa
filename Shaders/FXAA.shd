// Normal FXAA (i.e. not compute shader). Edited by Carl Stejmar

struct VS_OUT
{
	float4 Position : SV_Position;
	float2 TexCoord0 : TEXCOORD0;
	float4 TexCoord1 : TEXCOORD1;
};

#define CB(n, name, reg, count)\
		cbuffer cb##name : register(b##n)\
		{\
			float4 name[count];\
		}

[Vertex shader]

CB(0, VsConsts, 0, 1);
static const float2 HalfPixelSize = VsConsts[0].xy;

VS_OUT main(uint VertexID : SV_VertexID)
{
	VS_OUT Out;

	// Produce a fullscreen triangle
	Out.Position.x = (VertexID == 0) ? 3.0f : -1.0f;
	Out.Position.y = (VertexID == 2) ? 3.0f : -1.0f;
	Out.Position.zw = 1.0f;
	Out.TexCoord0.xy = Out.Position.xy * float2(0.5f, -0.5f) + 0.5f;
	Out.TexCoord1.xy = Out.TexCoord0.xy - HalfPixelSize;
	Out.TexCoord1.zw = Out.TexCoord0.xy + HalfPixelSize;

	return Out;
}

[Fragment shader]

#define FXAA_PRESET 3
#define FXAA_HLSL_4 1
//#define FXAA_DEBUG			 0
//#define FXAA_DEBUG_PAIR        1
//#define FXAA_DEBUG_PASSTHROUGH 1
//#define FXAA_DEBUG_HORZVERT    1
//#define FXAA_DEBUG_NEGPOS      1
//#define FXAA_DEBUG_OFFSET      1


//#define FXAA_GREEN_AS_LUMA 1
#define FXAA_EARLY_EXIT 1
#include "Fxaa4_0D.h"

struct PS_IN
{
	float4 Position : SV_Position;
	float2 TexCoord0 : TEXCOORD0;
	float4 TexCoord1 : TEXCOORD1;
};

CB(0, PsConsts, 0, 4);
static const float4 ConsoleRcpFrameOpt = PsConsts[0];
static const float4 ConsoleRcpFrameOpt2 = PsConsts[1];
static const float4 Console360RcpFrameOpt2 = PsConsts[2];
static const float4 Console360ConstDir = PsConsts[3];

Texture2D Tex : register(t0);
SamplerState Samp : register(s0);

void main(PS_IN In, out half4 Out : SV_Target)
{
	FxaaTex Scene;
	Scene.smpl = Samp;
	Scene.tex = Tex;

	Out.xyz = FxaaPixelShader(
		In.TexCoord0.xy,
		Scene,
		saturate(ConsoleRcpFrameOpt.zw*4.f)
		);
	Out.a = 0.f;
}
