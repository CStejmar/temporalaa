//_________________________________________________________//
// File: T2MSAAShaderIncludes.h							   //
// Language: HLSL										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: T2MSAA shader functions					   //	
//_________________________________________________________//

#define PI 3.14159265358979323846f

float Luma(in float3 clr)
{
	return dot(clr, float3(0.299f, 0.587f, 0.114f));
}

float LinearizeDepth(float z, float2 nfConst)
{
	return 1.f / (z * nfConst.x + nfConst.y);
}

float4 UVPosition(float4 homogeneousPos)
{
	// float4(x,y,z,1)
	homogeneousPos /= homogeneousPos.w;

	// -> [0, 1]
	homogeneousPos.xy *= float2(0.5f, -0.5f);
	homogeneousPos.xy += float2(0.5f, 0.5f);

	return homogeneousPos;
}

float updateSmoothingFactor(uint samples) // alpha = smoothingfactor
{
	float a = 1.0f / (float)(samples + 1);
	return a;
}

float tent(float2 radius, float2 signedDistance)
{
	return clamp(1 - abs(signedDistance.x) / radius.x, 0, 1)*clamp(1 - abs(signedDistance.y) / radius.y, 0, 1);
}

float2 DecodeObjectVelocityTexture(float2 tex_velocity)
{
	// Scale/bias from [0, 1] -> [-1, 1].
	tex_velocity = tex_velocity * 2.0f - 1.0f;

	// Fix up render format scaling.
	return tex_velocity / (8.0f);
}

//float4 bilinearSample(float2 positionSS, Texture2D<float4> texture)
//{
//	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
//	float3 sample = float3(0.0f, 0.0f, 0.0f);
//	float weight = 0.0f;
//
//	float2 iPositionSS = floor(positionSS); // Integer coordinate
//
//	float r = 1.0f;
//
//
//	for (uint d = 0; d < 4; d++)
//	{
//		float w_d = tent(r, positionSS - (iPositionSS + delta[d]));
//		weight += w_d;
//
//		sample += w_d*texture[int3(iPositionSS + delta[d], 0)].rgb;
//	}
//
//	if (weight != 0.0f)
//		sample = sample / weight;
//
//	return float4(sample, 1.0f);
//}

float3 bilinearSampleMS(float2 positionSS, Texture2DMS<float4, 2> frameTexture, int ms)
{
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float3 sample = float3(0.0f, 0.0f, 0.0f);

	float2 iPositionSS = floor(positionSS); // Integer coordinate

	float r = 1.0f;

	for (int i = 0; i < ms; i++)
	{
		float3 tempSample = float3(0.0f, 0.0f, 0.0f);
		float weight = 0.0f;

		for (uint d = 0; d < 4; d++)
		{
			float2 dist;
			if (i == 1)
				dist = positionSS - (iPositionSS + float2(0.25f, 0.25f) + delta[d]);
			else
				dist = positionSS - (iPositionSS + float2(0.75f, 0.75f) + delta[d]);

			float w_d = tent(r, dist);
			weight += w_d;

			tempSample += w_d*frameTexture.Load(int2(iPositionSS + delta[d]), i).rgb;
		}

		if (weight != 0.0f)
			tempSample = tempSample / weight;

		sample += tempSample / (float)ms;
	}

	return sample;
}

float bilinearSampleMSDepth(float2 positionSS, Texture2DMS<float, 2> depthTexture, int ms)
{
	float2 delta[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
	float sample = 0.0f;

	float2 iPositionSS = floor(positionSS); // Integer coordinate

	float r = 1.0f;

	for (int i = 0; i < ms; i++)
	{
		float tempSample = 0.0f;
		float weight = 0.0f;

		for (int d = 0; d < 4; d++)
		{
			float w_d = tent(r, positionSS - (iPositionSS + delta[d]));
			weight += w_d;

			tempSample += w_d*depthTexture.Load(int2(iPositionSS + delta[d]), i).r;
		}

		if (weight != 0.0f)
			tempSample = tempSample / weight;

		sample += tempSample / (float)ms;
	}

	return sample;
}

struct Output
{
	float4 color;
	uint samples;
};

float blurThreshold(float alpha, float2 velocity)
{
	float sigma = 0.5f;
	float result = sigma*sigma + ((1.0f - alpha) / alpha)*((velocity.x*(1.0f - velocity.x) + velocity.y*(1.0f - velocity.y)) / 2.0f);
	result = saturate(result);

	return  result;
}

Output updateT2MSAA(Texture2DMS<float4> currentFrameMS, Texture2D<float4> previousFrame, Texture2D<float4> velocityTexture, Texture2DMS<float> prevDepthTextureMS, Texture2DMS<float> depthTextureMS, SamplerState bilinearSampler, SamplerState pointSampler, float2 textureSize, float2 currentPositionUV, float2 linearDepthParams, int msaaSamples, uint samples)
{
	float3 currentColor = float3(0.0f, 0.0f, 0.0f);
	float3 previousColor = float3(0.0f, 0.0f, 0.0f);
	float4 frame = float4(0.0f, 0.0f, 0.0f, 0.0f);

	uint sampleUpdate = samples;

	float2 currentPositionSS = currentPositionUV*textureSize;

	float2 velocity = velocityTexture.Sample(pointSampler, currentPositionUV).xy;

	float2 previousPositionUV = currentPositionUV - velocity;
	float2 previousPositionSS = previousPositionUV*textureSize;

	for (int subSampleIdx = 0; subSampleIdx < msaaSamples; ++subSampleIdx)
	{
		//currentColor += bilinearSampleMS(currentPositionSS, currentFrameMS, msaaSamples).rgb / (float)msaaSamples;
		currentColor += currentFrameMS.Load(int2(currentPositionSS), subSampleIdx).rgb / (float)msaaSamples;
	}

	previousColor = previousFrame.Sample(bilinearSampler, previousPositionUV).rgb;
	
	float prevDepth = bilinearSampleMSDepth(previousPositionSS, prevDepthTextureMS, msaaSamples);
	float currDepth = bilinearSampleMSDepth(currentPositionSS, depthTextureMS, msaaSamples);

	float prevLinDepth = LinearizeDepth(prevDepth, linearDepthParams);
	float currLinDepth = LinearizeDepth(currDepth, linearDepthParams);

	bool miss = false;
	float2 pixelRadius = max(ddx(previousPositionSS), ddy(previousPositionSS));// *0.5f;

	// Miss if the reprojected (previous) position is more than pixelRadius away from closest pixel (integer value)
	miss = miss || !all(abs(previousPositionSS - round(previousPositionSS)) <= pixelRadius);

	// Miss if the depth difference is over a dynamic and linear threshold
	miss = miss || (currLinDepth - prevLinDepth) > (0.08f*currLinDepth);

	// Miss if the previous position is outside of the frustum(screen)
	miss = miss || (saturate(previousPositionUV.x) != previousPositionUV.x || saturate(previousPositionUV.y) != previousPositionUV.y);

	if(miss)
	{
		frame = float4(currentColor, 1.0f); //frame = float4(1.0f, 0.0f, 0.0f, 1.0f); //
		sampleUpdate = 1;
	}
	else // Do some weighting with previous frame
	{
		float alpha = updateSmoothingFactor(sampleUpdate);
		float tb = (velocity.x != 0.0f || velocity.y != 0.0f) ? blurThreshold(alpha, abs(velocity)) : 0.0f;
		alpha = max(alpha, tb);
		
		float weightCurrent = alpha*(1.0f / (1.0f + Luma(currentColor)));
		float weightPrevious = (1 - alpha)*(1.0f / (1.0f + Luma(previousColor)));

		frame = (weightCurrent*float4(currentColor, 1.0f) + weightPrevious*float4(previousColor, 1.0f)) / (weightCurrent + weightPrevious);

		if (alpha > tb)
			sampleUpdate += 1;
	}

	/*if (sampleUpdate > 3 && velocity.x != 0.0f && velocity.y != 0.0f)
		sampleUpdate = 3;*/

	Output result;
	result.color = frame;
	result.samples = sampleUpdate;

	return result;
}

Output T2MSAADebug(int msaaSamples, Texture2DMS<float4> currentFrame, float2 textureSize, float2 currentPositionUV, float4 worldPosition)
{
	float2 currentPositionSS = currentPositionUV*textureSize;
	float3 currentColor = float3(0.0f, 0.0f, 0.0f);

	for (int subSampleIdx = 0; subSampleIdx < msaaSamples; ++subSampleIdx)
	{
		currentColor += currentFrame.Load(uint2(currentPositionSS), subSampleIdx).rgb / (float)msaaSamples;
	}

	Output result;
	result.color = float4(currentColor, 1.0f);

	return result;
}
