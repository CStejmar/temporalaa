//_________________________________________________________//
// File: VelocityBuffer.shd								   //
// Language: HLSL										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-22								   //
// Description: First velocity shader pass				   //
//_________________________________________________________//

struct VS_IN
{
	float3 Position : POSITION0;
};

struct VS_OUT
{
	float4 Pos : SV_Position;
	float4 CurrentPos : TEXCOORD0;
	float4 PreviousPos : TEXCOORD1;
};

[Vertex shader]

float4x4 WorldViewProjection;
float4x4 PreviousWorldViewProjection;
float4x4	ViewProjInv;
float4		InvFar;

VS_OUT  main(VS_IN In)
{
	VS_OUT Out;

	// Transform position into screen coordinates
	Out.Pos = mul(WorldViewProjection, float4(In.Position.xyz, 1)); // Changed order due to differences in the frameworks
	Out.CurrentPos = Out.Pos;

	Out.PreviousPos = mul(PreviousWorldViewProjection, float4(In.Position.xyz, 1)); // Changed order due to differences in the frameworks

	return Out;
}

[Fragment shader]

//float2 EncodeObjectVelocityTexture(float2 velocity)
//{
//	// Apply scaling for better utilization of render target format.
//	velocity = clamp(velocity * 8.0f, -1.0f, 1.0f);
//
//	// Remap final velocities to [0, 1].
//	return velocity * 0.5f + 0.5f;
//}

float4 CalculateMotionBlurVectors(float4 previousPos, float4 currentPos)
{
	// Remap position to [0, 1] screen space.
	previousPos.xy = previousPos.xy / previousPos.w;
	previousPos.xy *= float2(0.5f, -0.5f);
	previousPos.xy += float2(0.5f, 0.5f);

	// Remap position to [0, 1] screen space.
	currentPos.xy = currentPos.xy / currentPos.w;
	currentPos.xy *= float2(0.5f, -0.5f);
	currentPos.xy += float2(0.5f, 0.5f);

	float2 motion_vector = currentPos.xy - previousPos.xy;

	// Store w coordinate so we can verify that the velocity buffer values are front-most.
	// 60 units in "game" corresponds to 6 meters (height of house) ==> 60/6 = 10 units/meter ==> 1 unit/decimeter. far = 2000.0f (units) -> 2000 decimeter -> we have 200 meters view range
	// JC3: 0.01f gives us 100 meters in 40cm steps (100/256)// 2^8=256, 2^16 = 65536
	return float4(motion_vector.xy, 0.005f*currentPos.w, 0.0f);
}

struct PS_IN
{
	float4 Pos : SV_Position;
	float4 CurrentPos : TEXCOORD0;
	float4 PreviousPos : TEXCOORD1;
};

void main(PS_IN In, out float4 Color : SV_Target)
{
		Color = CalculateMotionBlurVectors(In.PreviousPos, In.CurrentPos);
}
