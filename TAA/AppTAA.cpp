
/* * * * * * * * * * * * * Author's note * * * * * * * * * * * *\
*   _       _   _       _   _       _   _       _     _ _ _ _   *
*  |_|     |_| |_|     |_| |_|_   _|_| |_|     |_|  _|_|_|_|_|  *
*  |_|_ _ _|_| |_|     |_| |_|_|_|_|_| |_|     |_| |_|_ _ _     *
*  |_|_|_|_|_| |_|     |_| |_| |_| |_| |_|     |_|   |_|_|_|_   *
*  |_|     |_| |_|_ _ _|_| |_|     |_| |_|_ _ _|_|  _ _ _ _|_|  *
*  |_|     |_|   |_|_|_|   |_|     |_|   |_|_|_|   |_|_|_|_|    *
*                                                               *
*                     http://www.humus.name                     *
*                                                                *
* This file is a part of the work done by Humus. You are free to   *
* use the code in any way you like, modified, unmodified or copied   *
* into your own work. However, I expect you to respect these points:  *
*  - If you use this file and its contents unmodified, or use a major *
*    part of this file, please credit the author and leave this note. *
*  - For use in anything commercial, please request my approval.     *
*  - Share your work and ideas too as much as you can.             *
*                                                                *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Edited by Carl Stejmar

#include "AppTAA.h"

BaseApp *app = new App();

void App::moveCamera(const float3 &dir)
{
	float3 newPos = camPos + dir * (speed * frameTime);
	camPos = newPos;
}

void App::resetCamera()
{
	camPos = vec3(2.6f, 27.6f, 79.1f);
	wx = 0.0f;
	wy = -1.57f;
}

bool App::onKey(const uint key, const bool pressed)
{
	if (D3D11App::onKey(key, pressed)) return true;

	if (pressed)
	{
		if (m_UseTAA->isChecked())
		{
			if (key == KEY_MULTIPLY || key == KEY_4)
			{
				t2msaa->reset();
			}
		}
		if (m_UseASAA->isChecked())
		{
			if (key == KEY_MULTIPLY || key == KEY_5)
			{
				asaa->reset();
				//asaaexp->reset();
			}
		}

		if (key == KEY_1)
		{
			if (!m_UseFXAACS->isChecked() && !m_UseFXAA->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked() && !m_UseMSAA->isChecked())
				m_UseGBAA->setChecked(!m_UseGBAA->isChecked());
		}
		if (key == KEY_2)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked() && !m_UseMSAA->isChecked())
				m_UseFXAA->setChecked(!m_UseFXAA->isChecked());
		}
		if (key == KEY_3)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked() && !m_UseMSAA->isChecked())
				m_UseFXAACS->setChecked(!m_UseFXAACS->isChecked());
		}
		if (key == KEY_4)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseASAA->isChecked() && !m_UseMSAA->isChecked())
				m_UseTAA->setChecked(!m_UseTAA->isChecked());
		}
		if (key == KEY_5)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseTAA->isChecked() && !m_UseMSAA->isChecked())
				m_UseASAA->setChecked(!m_UseASAA->isChecked());
		}
		if (key == KEY_6)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked())
				m_UseMSAA->setChecked(!m_UseMSAA->isChecked());
			m_MSx = 2;
		}
		if (key == KEY_7)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked())
				m_UseMSAA->setChecked(!m_UseMSAA->isChecked());
			m_MSx = 4;
		}
		if (key == KEY_8)
		{
			if (!m_UseGBAA->isChecked() && !m_UseFXAA->isChecked() && !m_UseFXAACS->isChecked() && !m_UseTAA->isChecked() && !m_UseASAA->isChecked())
				m_UseMSAA->setChecked(!m_UseMSAA->isChecked());
			m_MSx = 8;
		}

		if (m_UseFXAA->isChecked())
		{
			if (key == KEY_ADD)
			{
				fxaa->increaseSubPixelSharpness();
			}
			if (key == KEY_SUBTRACT)
			{
				fxaa->decreaseSubPixelSharpness();
			}
		}
		if (m_UseFXAACS->isChecked())
		{
			if (key == KEY_MULTIPLY)
			{
				if (fxaacs->b_paramSwitch)
					fxaacs->b_paramSwitch = false;
				else
					fxaacs->b_paramSwitch = true;
			}
			if (key == KEY_ADD)
			{
				if (fxaacs->b_paramSwitch)
				{
					fxaacs->increaseContrastThreshold();
				}
				else
				{
					fxaacs->increaseSubpixelRemoval();
				}
			}
			if (key == KEY_SUBTRACT)
			{
				if (fxaacs->b_paramSwitch)
				{
					fxaacs->decreaseContrastThreshold();
				}
				else
				{
					fxaacs->decreaseSubpixelRemoval();
				}
			}
		}

		if (key == KEY_DIVIDE)
		{
			if (m_debugDraw)
				m_debugDraw = false;
			else
				m_debugDraw = true;
		}

		if (key == KEY_9)
		{
			m_saveSequence = true;
			m_firstFrame = m_FrameCount;
		}
	}
	return false;
}

void App::onSize(const int w, const int h)
{
	D3D11App::onSize(w, h);

	if (renderer)
	{
		// Make sure render targets are the size of the window
		renderer->resizeRenderTarget(m_BaseRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_NormalRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_DepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_ResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_GeometryRT, w, h, 1, 1, 1);

		renderer->resizeRenderTarget(m_PrevResultRT, w, h, 1, 1, 1);

		renderer->resizeRenderTarget(m_MS2xResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS2xPrevResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS2xDepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS2xPrevDepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS2xBaseRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS2xNormalRT, w, h, 1, 1, 1);

		renderer->resizeRenderTarget(m_MS4xResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS4xDepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS4xBaseRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS4xNormalRT, w, h, 1, 1, 1);

		renderer->resizeRenderTarget(m_MS8xResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS8xDepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS8xBaseRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MS8xNormalRT, w, h, 1, 1, 1);

		/*renderer->resizeRenderTarget(m_MSResultRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MSDepthRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MSBaseRT, w, h, 1, 1, 1);
		renderer->resizeRenderTarget(m_MSNormalRT, w, h, 1, 1, 1);*/

		fxaacs->resize(w, h);
		t2msaa->resize(w, h);
		asaa->resize(w, h);

		asaaexp->resize(w, h);

		velocity->resize(w, h);
	}
}

bool App::init()
{
	m_firstFrame = 0;
	m_saveSequence = false;
	m_MS = 1;
	m_MultiSampling = false;
	averageFrameTime = 0.0f;
	first = true;
	index = 0;
	m_aaMode = 0;
	m_counter = 0;

	// Init projection matrix with reversed depth (far near have switched position)
	projectionMat4 = toD3DProjection(perspectiveMatrixY(1.2f, width, height, far_plane, near_plane));
	projectionViewMat4 = identity4();
	prevProjectionViewMat4 = identity4();

	// No framework-created depth buffer
	depthBits = 0;

	// Init static models
	m_TownMdl = new DynamicModel("oldTownBlock.obj", false, true, vec3(0.0f, -57.0f, 0.0f));

	// Init skybox model
	m_SkyboxMdl = new StaticModel("skybox.obj");

	// Init dynamic models
	m_TRexMdl = new DynamicModel("rex.obj", vec3(-250.0f, 28.0f, -250.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.2f, 0.2f, 0.2f));
	m_AppleMdl = new DynamicModel("apple.obj", false, true, vec3(4.0f, 55.0f, -2.0f), vec3(3.14f / 2.0f, 0.0f, 0.0f), vec3(0.2f, 0.2f, 0.2f));
	m_AppleMdl2 = new DynamicModel(m_AppleMdl->getModel(), "apple.obj", vec3(-8.0f, 70.0f, 264.0f)); // Does not load model again. Uses a pointer to already loaded model.

	dynamicModels.push_back(m_TRexMdl);
	dynamicModels.push_back(m_AppleMdl);
	dynamicModels.push_back(m_AppleMdl2);

	dynamicModels.push_back(m_TownMdl);

	// Create light sphere for deferred shading
	m_SphereMdl = new Model();
	m_SphereMdl->createSphere(3);

	// Initialize all lights
	m_Lights[0].position = float3(52.0f, 20.5f, 222.4f);	m_Lights[0].radius = 140.0f;
	m_Lights[1].position = float3(62.7f, 20.5f, 126.9f);	m_Lights[1].radius = 140.0f;
	m_Lights[2].position = float3(89.4f, 20.5f, 61.9f);		m_Lights[2].radius = 140.0f;
	m_Lights[3].position = float3(188.9f, 20.5f, 25.2f);	m_Lights[3].radius = 140.0f;
	m_Lights[4].position = float3(268.9f, 20.5f, -81.7f);	m_Lights[4].radius = 140.0f;
	m_Lights[5].position = float3(77.3f, 20.5f, -69.2f);	m_Lights[5].radius = 140.0f;
	m_Lights[6].position = float3(-25.1f, 20.5f, 16.1f);	m_Lights[6].radius = 140.0f;
	m_Lights[7].position = float3(-54.2f, 20.5f, 132.2f);	m_Lights[7].radius = 140.0f;
	m_Lights[8].position = float3(-62.0f, 20.5f, 260.5f);	m_Lights[8].radius = 140.0f;

	m_Lights[9].position = float3(20.0f, 120.0f, 20.0f);	m_Lights[9].radius = 650.0f;

	// Init GUI components
	int tab = configDialog->addTab("Anti-aliasing");
	configDialog->addWidget(tab, m_UseGBAA = new CheckBox(0, 0, 150, 36, "Use GBAA", false));
	configDialog->addWidget(tab, m_UseFXAA = new CheckBox(0, 40, 200, 36, "Use FXAA", false));
	configDialog->addWidget(tab, m_UseFXAACS = new CheckBox(0, 80, 250, 36, "Use FXAACS", false));
	configDialog->addWidget(tab, m_UseTAA = new CheckBox(0, 120, 300, 36, "Use TAA", false));
	configDialog->addWidget(tab, m_UseASAA = new CheckBox(0, 160, 350, 36, "Use ASAA", false));
	configDialog->addWidget(tab, m_UseMSAA = new CheckBox(0, 200, 400, 36, "Use MSAA", false));

	m_debugDraw = false; // Can be used for all AA methods

	return true;
}

void App::exit()
{
	// Static models
	delete m_SkyboxMdl;
	delete m_TownMdl;

	// Dynamic models
	delete m_TRexMdl;
	delete m_AppleMdl;
	delete m_AppleMdl2;

	// Deferred shading
	delete m_SphereMdl;

	// AA
	delete gbaa;
	delete fxaa;
	delete fxaacs;
	delete t2msaa;
	delete asaa;

	delete asaaexp;

	// Velocity
	delete velocity;
}

bool App::initAPI()
{
	// Override the user's MSAA settings and disable the control
	antiAlias->setEnabled(false);
	//return D3D11App::initAPI(D3D11, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB, DXGI_FORMAT_UNKNOWN, 1, NO_SETTING_CHANGE);
	return D3D11App::initAPI(D3D11, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_UNKNOWN, 1, NO_SETTING_CHANGE);
}

void App::exitAPI()
{
	D3D11App::exitAPI();
}

bool App::load()
{
	// Shaders
	if ((m_FillBuffers[0] = renderer->addShader((c_shaderPath + "FillBuffers.shd").c_str())) == SHADER_NONE) return false;
	if ((m_FillBuffers[1] = renderer->addShader((c_shaderPath + "FillBuffers.shd").c_str(), "#define ALPHA_TEST\n")) == SHADER_NONE) return false;
	if ((m_TextureShd = renderer->addShader((c_shaderPath + "Texture.shd").c_str())) == SHADER_NONE) return false;
	if ((m_SkyBoxShd = renderer->addShader((c_shaderPath + "SkyBox.shd").c_str())) == SHADER_NONE) return false;
	if ((m_AmbientShd = renderer->addShader((c_shaderPath + "Ambient.shd").c_str())) == SHADER_NONE) return false;
	if ((m_LightingShd = renderer->addShader((c_shaderPath + "Lighting.shd").c_str())) == SHADER_NONE) return false;

	if ((m_MSAmbientShd = renderer->addShader((c_shaderPath + "AmbientMS.shd").c_str())) == SHADER_NONE) return false;
	if ((m_MSLightingShd = renderer->addShader((c_shaderPath + "LightingMS.shd").c_str())) == SHADER_NONE) return false;

	// Velocity
	float2 linearDepthParams;
	linearDepthParams.x = 1.0f / near_plane - 1.0f / far_plane;
	linearDepthParams.y = 1.0f / far_plane;

	velocity = new Velocity(renderer, width, height, linearDepthParams, far_plane);

	// AA methods
	gbaa = new GBAA(renderer, "GBAA.shd", "GBAADebug.shd");
	fxaa = new FXAA(renderer, "FXAA.shd", "FXAADebug.shd");
	fxaacs = new FXAACS(renderer, "FXAACS/FXAAPass1.shd", width, height);
	t2msaa = new T2MSAA(renderer, width, height, linearDepthParams, far_plane, projectionMat4);
	asaa = new ASAA(renderer, width, height, linearDepthParams, far_plane, projectionMat4);
	asaaexp = new ASAAexp(renderer, width, height, linearDepthParams, far_plane, projectionMat4);

	// Samplerstates
	if ((m_BaseFilter = renderer->addSamplerState(TRILINEAR_ANISO, WRAP, WRAP, WRAP)) == SS_NONE) return false;
	if ((m_PointClamp = renderer->addSamplerState(NEAREST, CLAMP, CLAMP, CLAMP)) == SS_NONE) return false;

	// Main render targets
	if ((m_BaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE, SRGB)) == TEXTURE_NONE) return false;
	if ((m_NormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, 1, SS_NONE)) == TEXTURE_NONE) return false;
	if ((m_DepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D16, 1, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	if ((m_ResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE, USE_UAV)) == TEXTURE_NONE) return false;
	if ((m_GeometryRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RG16F, 1, SS_NONE)) == TEXTURE_NONE) return false;

	if ((m_PrevResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE, USE_UAV)) == TEXTURE_NONE) return false;

	// 2x MultiSampling render targets
	int ms2x = 2;
	if ((m_MS2xResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms2x, SS_NONE)) == TEXTURE_NONE) return false;
	if ((m_MS2xPrevResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms2x, SS_NONE)) == TEXTURE_NONE) return false;
	if ((m_MS2xDepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D24, ms2x, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	if ((m_MS2xPrevDepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D24, ms2x, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	if ((m_MS2xBaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms2x, SS_NONE, SRGB)) == TEXTURE_NONE) return false;
	if ((m_MS2xNormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, ms2x, SS_NONE)) == TEXTURE_NONE) return false;

	// 4x MultiSampling render targets
	int ms4x = 4;
	if ((m_MS4xResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms4x, SS_NONE)) == TEXTURE_NONE) return false;
	if ((m_MS4xDepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D24, ms4x, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	if ((m_MS4xBaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms4x, SS_NONE, SRGB)) == TEXTURE_NONE) return false;
	if ((m_MS4xNormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, ms4x, SS_NONE)) == TEXTURE_NONE) return false;

	// 8x MultiSampling render targets
	int ms8x = 8;
	if ((m_MS8xResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms8x, SS_NONE)) == TEXTURE_NONE) return false;
	if ((m_MS8xDepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D24, ms8x, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	if ((m_MS8xBaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms8x, SS_NONE, SRGB)) == TEXTURE_NONE) return false;
	if ((m_MS8xNormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, ms8x, SS_NONE)) == TEXTURE_NONE) return false;

	//// MultiSampling render targets
	//int ms = 2;
	//if ((m_MSResultRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms, SS_NONE)) == TEXTURE_NONE) return false;
	//if ((m_MSDepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D16, ms, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
	//if ((m_MSBaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, ms, SS_NONE, SRGB)) == TEXTURE_NONE) return false;
	//if ((m_MSNormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, ms, SS_NONE)) == TEXTURE_NONE) return false;

	// Blendstates
	if ((m_BlendAdd = renderer->addBlendState(ONE, ONE)) == BS_NONE) return false;

	// Depth states - use reversed depth (1 to 0) to improve precision
	if ((m_DepthTest = renderer->addDepthState(true, true, GEQUAL)) == DS_NONE) return false;
	if ((m_DepthAlways = renderer->addDepthState(true, true, ALWAYS)) == DS_NONE) return false;

	// Upload map to vertex/index buffer for light model
	if (!m_SphereMdl->makeDrawable(renderer, true, m_LightingShd)) return false;

	// Load objects and set their properties
	if (!m_SkyboxMdl->setModelProperties("", "", &m_SkyBoxShd, renderer, &m_BaseFilter, false, false, true))
		return false;

	// Town model textures
	std::vector<std::string> textureNames;
	textureNames.push_back("tex6.jpg"); // street. tex1: brick wall.		DONE
	textureNames.push_back("concrete.jpg"); // house base. tex2: stone floor
	textureNames.push_back("tex2.jpg"); // porch. tex3: dark wood			DONE
	textureNames.push_back("tex4.jpg"); // door. tex4: door				DONE
	textureNames.push_back("tex1.jpg"); // chimney. tex5: bump hand.		DONE
	textureNames.push_back("tex9.jpg"); // single window. tex6: street.	DONE
	textureNames.push_back("tex7.jpg"); // roof. tex7: roof.						DONE
	textureNames.push_back("tex8.jpg"); // under roof. tex8: wood floor
	textureNames.push_back("tex10.jpg"); // back and front of house. tex9: window. DONE
	textureNames.push_back("tex11.jpg"); // side of house. tex10: house wall door. DONE
	textureNames.push_back("lamp.jpg"); // low lamps. tex11: house wall
	textureNames.push_back("graySteel.jpg"); // low lamp poles
	textureNames.push_back("graySteel.jpg"); // high lamps
	textureNames.push_back("graySteel.jpg"); // high lamps: cylinder
	textureNames.push_back("tex3.jpg"); // street lamp poles
	textureNames.push_back("graySteel.jpg"); // street lamp wires

	std::vector<std::string> bumpMapNames;
	bumpMapNames.assign(textureNames.size(), "");

	if (!m_TownMdl->setModelPropertiesVec(textureNames, bumpMapNames, &m_TextureShd, renderer, &m_BaseFilter))
		return false;

	if (!m_TRexMdl->setModelProperties("snow.tga", "", &m_TextureShd, renderer, &m_BaseFilter, true, false, false))
		return false;

	if (!m_AppleMdl->setModelProperties("tex2.jpg", "", &m_TextureShd, renderer, &m_BaseFilter, true, false, false))
		return false;

	if (!m_AppleMdl2->setModelProperties("tex2.jpg", "", &m_TextureShd, renderer, &m_BaseFilter, true, false, false))
		return false;

	return true;
}

void App::unload()
{

}

void App::drawFrame()
{
	TextureID target, depth, base, normal, geometry;
	ShaderID light, ambient, modelShader;
	if (m_UseFXAA->isChecked() || m_UseFXAACS->isChecked() || m_UseASAA->isChecked())
	{
		// Render targets
		target = m_ResultRT;
		depth = m_DepthRT;
		base = m_BaseRT;
		normal = m_NormalRT;
		geometry = TEXTURE_NONE;

		// Shaders
		light = m_LightingShd;
		ambient = m_AmbientShd;
		modelShader = m_TextureShd;

		// Multisampling
		m_MultiSampling = false;
		m_MS = 1;
	}
	else if (m_UseGBAA->isChecked())
	{
		// Render targets
		target = m_ResultRT;
		depth = m_DepthRT;
		base = m_BaseRT;
		normal = m_NormalRT;
		geometry = m_GeometryRT;

		// Shaders
		light = m_LightingShd;
		ambient = m_AmbientShd;
		modelShader = m_FillBuffers[0];

		// Multisampling
		m_MultiSampling = false;
		m_MS = 1;

	}
	else if (m_UseTAA->isChecked())
	{
		// Render targets
		target = m_MS2xResultRT;
		depth = m_MS2xDepthRT;
		base = m_MS2xBaseRT;
		normal = m_MS2xNormalRT;
		geometry = TEXTURE_NONE;

		// Shaders
		light = m_MSLightingShd;
		ambient = m_MSAmbientShd;
		modelShader = m_TextureShd;

		// Multisampling
		m_MultiSampling = true;
		m_MS = 2;
	}
	else if (m_UseMSAA->isChecked())
	{
		if (m_MSx == 2)
		{
			// 2x render targets
			target = m_MS2xResultRT;
			depth = m_MS2xDepthRT;
			base = m_MS2xBaseRT;
			normal = m_MS2xNormalRT;
			geometry = TEXTURE_NONE;
		}
		else if (m_MSx == 4)
		{
			// 4x render targets
			target = m_MS4xResultRT;
			depth = m_MS4xDepthRT;
			base = m_MS4xBaseRT;
			normal = m_MS4xNormalRT;
			geometry = TEXTURE_NONE;
		}
		else if (m_MSx == 8)
		{
			// 8x render targets
			target = m_MS8xResultRT;
			depth = m_MS8xDepthRT;
			base = m_MS8xBaseRT;
			normal = m_MS8xNormalRT;
			geometry = TEXTURE_NONE;
		}

		// Shaders
		light = m_MSLightingShd;
		ambient = m_MSAmbientShd;
		modelShader = m_TextureShd;

		// Multisampling
		m_MultiSampling = true;
		m_MS = m_MSx;
	}
	else
	{
		//Render targets
		target = FB_COLOR;
		depth = m_DepthRT;
		base = m_BaseRT;
		normal = m_NormalRT;
		geometry = TEXTURE_NONE;

		// Shaders
		light = m_LightingShd;
		ambient = m_AmbientShd;
		modelShader = m_TextureShd;

		// Multisampling
		m_MultiSampling = false;
		m_MS = 1;

		m_aaMode = 0;
	}

	// Resets the shader depending on which AA method that is used
	for (uint m = 0; m < dynamicModels.size(); m++)
	{
		dynamicModels.at(m)->setShader(&modelShader);
	}

	float2 quadrant[4] = { float2(-0.25f, -0.25f), float2(-0.25f, 0.25f), float2(0.25f, -0.25f), float2(0.25f, 0.25f) };

	mat4 viewMat4 = rotateXY(-wx, -wy);

	mat4 currentProjection = projectionMat4;
	mat4 currentProjectionNoG = projectionMat4;
	mat4 skyboxMat4 = currentProjection*viewMat4; // Without translation

	if (m_UseTAA->isChecked())
	{
		//___ Jittering of the projection matrix___
		float2 screenSpaceOffset = m_FrameCount % 2 == 0 ? float2(-0.25f, 0.25f) : float2(0.25f, -0.25f);

		const float offsetX = (2.0f*screenSpaceOffset.x) / (float)width;
		const float offsetY = (-2.0f*screenSpaceOffset.y) / (float)height;

		mat4 translationMat = translate(float3(offsetX, offsetY, 0.0f));
		currentProjection = currentProjection*translationMat; // Multiplication order should be changed to get the desired result! See thesis.
	}

	if (m_UseASAA->isChecked())
	{
		float2 g = asaa->getGaussianOffset((int)round(averageFrameTime * 1000.0f));

		float2 screenSpaceOffset = quadrant[index];
		mat4 translationMat = translate(float3(2.0f*(screenSpaceOffset.x + g.x) / (float)width, -2.0f*(screenSpaceOffset.y + g.y) / (float)height, 0.0f));
		currentProjection = currentProjection*translationMat; // Multiplication order should be changed to get the desired result! See thesis.

		mat4 translationMatNoG = translate(float3(2.0f*screenSpaceOffset.x / (float)width, -2.0f*screenSpaceOffset.y / (float)height, 0.0f));
		currentProjectionNoG = currentProjectionNoG*translationMatNoG; // Multiplication order should be changed to get the desired result! See thesis.
	}

	viewMat4.translate(-camPos);

	projectionViewMat4 = currentProjection * viewMat4;

	// Pre-scale-bias the matrix so we can use the screen position directly
	mat4 projectionViewMat4Inv = (!projectionViewMat4) * (translate(-1.0f, 1.0f, 0.0f) * scale(2.0f, -2.0f, 1.0f));

	Direct3D11Renderer *d3D11_renderer = (Direct3D11Renderer *)renderer;

	if (m_UseGBAA->isChecked())
	{
		TextureID bufferRTs[] = { base, normal, geometry };
		renderer->changeRenderTargets(bufferRTs, elementsOf(bufferRTs), depth);
	}
	else
	{
		TextureID bufferRTs[] = { base, normal };
		renderer->changeRenderTargets(bufferRTs, elementsOf(bufferRTs), depth);
	}

	// Clear to 0.5f, indicating that there is no edge cutting through this pixel. (GBAA)
	d3D11_renderer->clearRenderTarget(m_GeometryRT, float4(0.5f, 0.5f, 0.5f, 0.5f));
	d3D11_renderer->clearRenderTarget(m_NormalRT, float4(0.5f, 0.5f, 0.5f, 0.5f));
	d3D11_renderer->clearRenderTarget(m_BaseRT, float4(0.5f, 0.5f, 0.5f, 0.5f));
	d3D11_renderer->clearDepthTarget(m_DepthRT, 0.0f);

	// Clear depth for ms render targets
	d3D11_renderer->clearDepthTarget(m_MS2xDepthRT, 0.0f);
	d3D11_renderer->clearDepthTarget(m_MS4xDepthRT, 0.0f);
	d3D11_renderer->clearDepthTarget(m_MS8xDepthRT, 0.0f);
	//d3D11_renderer->clearDepthTarget(m_MSDepthRT, 0.0f);

	/*
	Main scene pass.
	This is where the buffers are filled for the later deferred passes.
	*/
	float4 scale_bias(0.5f * width, -0.5f * height, 0.5f * width, 0.5f * height);

	//______________Draw models_______________
	m_TownMdl->updatePlacement(index);
	m_TownMdl->drawModelVec(m_DepthTest, cullBack, projectionViewMat4, scale_bias, m_UseGBAA->isChecked());

	// This scaling is added because for the first frames the frameTime is varying a lot. This makes dynamic objects start at the same position every run.
	float timeScaling;
	if (m_FrameCount < 3)
		timeScaling = 0;
	else
		timeScaling = frameTime;

	// Draw dynamic models
	m_TRexMdl->setVelocity(translate(1000 * timeScaling*cos(time) / 50.0f, 0.0f, 1000 * timeScaling*sin(time) / 50.0f));
	m_TRexMdl->setRotation(rotateY(time));
	m_TRexMdl->updatePlacement(index);
	m_TRexMdl->drawModel(m_DepthTest, cullBack, projectionViewMat4, scale_bias, m_UseGBAA->isChecked(), vec4(1.0f, 0.0f, 0.0f, 1.0f));

	m_AppleMdl->setVelocity(translate(0.0f, 1000 * timeScaling*sin(time*4.0f) / 100.0f, 0.0f));
	m_AppleMdl->updatePlacement(index);
	m_AppleMdl->drawModel(m_DepthTest, cullBack, projectionViewMat4, scale_bias, m_UseGBAA->isChecked());

	m_AppleMdl2->setVelocity(translate(0.0f, 0.0f, 1000 * timeScaling*sin(time*10.0f) / 20.0f));
	m_AppleMdl2->updatePlacement(index);
	m_AppleMdl2->drawModel(m_DepthTest, cullBack, projectionViewMat4, scale_bias, m_UseGBAA->isChecked());

	//______________Velocity calc._______________
	if (!first)
	{
		// Fill velocity buffer
		velocity->setViewProjMatrix(projectionViewMat4);
		velocity->setPrevViewProjMatrix(prevProjectionViewMat4);

		d3D11_renderer->clearRenderTarget(velocity->getVelocityBuffer(), float4(0.0f, 0.0f, 1.0f, 0.0f)); // Clear to 0.5f for x and y, wich corresponds to 0 for unsigned velocities.
		d3D11_renderer->clearRenderTarget(velocity->getVelocityTexture(), float4(0.0f, 0.0f, 0.0f, 0.0f));

		renderer->changeRenderTarget(velocity->getVelocityBuffer());
		for (uint i = 0; i < dynamicModels.size() - 1; i++) // m_TownMdl is not included here, that is why dynamicModels.size() - 1 is the limit
		{
			velocity->calculateVelocityBuffer(dynamicModels.at(i), m_DepthTest, cullBack);
		}

		// Fill velocity texture
		renderer->changeRenderTarget(velocity->getVelocityTexture());
		velocity->calculateVelocityTexture(depth, m_DepthTest, cullBack, m_PointClamp, camPos, m_MultiSampling, m_MS);
	}
	//_________________________________________

	renderer->changeRenderTarget(target, TEXTURE_NONE);

	{
		/*
		Deferred ambient pass.
		*/
		renderer->reset();
		renderer->setRasterizerState(cullNone);
		renderer->setDepthState(noDepthTest);
		renderer->setShader(ambient);
		renderer->setTexture("Base", base);

		if (m_MS != 1)
			renderer->setShaderConstant1i("MS", m_MS);

		renderer->apply();

		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		context->Draw(3, 0);
		renderer->addVertices(3);
		renderer->addDrawCalls(1);

		/*
		Deferred lighting pass.
		*/
		renderer->reset();
		renderer->setDepthState(noDepthTest);
		renderer->setShader(light);
		renderer->setRasterizerState(cullFront);
		renderer->setBlendState(m_BlendAdd);
		renderer->setShaderConstant4x4f("ViewProj", projectionViewMat4);
		renderer->setShaderConstant4x4f("ViewProjInv", projectionViewMat4Inv * scale(1.0f / width, 1.0f / height, 1.0f));
		renderer->setShaderConstant3f("CamPos", camPos);

		if (m_MS != 1)
			renderer->setShaderConstant1i("MS", m_MS);

		renderer->setTexture("Base", base);
		renderer->setTexture("Normal", normal);
		renderer->setTexture("Depth", depth);
		renderer->apply();

		float2 zw = currentProjection.rows[2].zw();
		for (uint i = 0; i < LIGHT_COUNT; i++)
		{
			float3 lightPos = m_Lights[i].position;
			float radius = m_Lights[i].radius;
			float invRadius = 1.0f / radius;

			// Compute z-bounds
			float4 lPos = viewMat4 * float4(lightPos, 1.0f);
			float z1 = lPos.z + radius;

			if (z1 > near_plane)
			{
				float z0 = max(lPos.z - radius, near_plane);

				float2 zBounds;
				zBounds.y = saturate(zw.x + zw.y / z0);
				zBounds.x = saturate(zw.x + zw.y / z1);

				renderer->setShaderConstant3f("LightPos", lightPos);
				renderer->setShaderConstant1f("Radius", radius);
				renderer->setShaderConstant1f("InvRadius", invRadius);
				renderer->setShaderConstant2f("ZBounds", zBounds);
				renderer->applyConstants();

				m_SphereMdl->draw(renderer);
			}
		}
	}


	/*if (!m_UseASAA->isChecked()) // Used when the Reversed reprojection method is enabled for ASAA
	{*/
	renderer->changeRenderTarget(target, depth);

	// Skybox
	m_SkyboxMdl->drawModel(m_DepthTest, cullFront, skyboxMat4, scale_bias, m_UseGBAA->isChecked());
	//}


	//// Test of velocity
	//if (!first && m_UseTAA->isChecked())
	//{
	//	// As implemented now the velocity texture can not be shown on screen as the formats do not match, use older version
	//	context->CopyResource(backBuffer, ((Direct3D11Renderer*)renderer)->getResource(velocity->getVelocityTexture()));
	//	renderer->changeToMainFramebuffer();
	//}

	/*
	Geometry Buffer Anti-Aliasing
	*/
	if (m_UseGBAA->isChecked())
	{
		m_GBAA = true;
		m_aaMode = 1;

		gbaa->apply(target, geometry, width, height, m_debugDraw, noDepthTest, cullNone, linearClamp, m_PointClamp);
	}
	else
		m_GBAA = false;

	/*
	Fast approXimate Anti-Aliasing
	*/
	if (m_UseFXAA->isChecked())
	{
		m_FXAA = true;
		m_aaMode = 2;

		fxaa->apply(target, width, height, m_debugDraw, noDepthTest, cullNone, linearClamp);
	}
	else
		m_FXAA = false;

	/*
	Fast approXimate Anti-Aliasing implemented on compute shader
	*/
	if (m_UseFXAACS->isChecked())
	{
		m_FXAACS = true;
		m_aaMode = 3;

		fxaacs->apply(target, context, backBuffer, width, height, m_debugDraw, noDepthTest, cullNone, linearClamp);
	}
	else
		m_FXAACS = false;

	/*
	Temporal Anti-Aliasing with 2x Multisampling and accumulation buffer
	*/
	if (m_UseTAA->isChecked())
	{
		m_TAA = true;
		m_aaMode = 4;

		//_______________T2MSAA_______________
		t2msaa->setViewMatrix(viewMat4);
		t2msaa->setProjectionMatrix(currentProjection);
		t2msaa->apply(target, m_PrevResultRT, depth, m_MS2xPrevDepthRT, velocity->getVelocityTexture(), context, noDepthTest, cullNone, m_PointClamp, m_MS, width, height);
		context->CopyResource(backBuffer, ((Direct3D11Renderer*)renderer)->getResource(t2msaa->getRT()));
		renderer->changeToMainFramebuffer();

		context->CopyResource(((Direct3D11Renderer*)renderer)->getResource(m_MS2xPrevDepthRT), ((Direct3D11Renderer*)renderer)->getResource(depth));
		//___________________________________
	}
	else
		m_TAA = false;

	/*
	Amortized Supersampling Anti-Aliasing using 4 subpixel buffers
	*/
	if (m_UseASAA->isChecked())
	{
		m_ASAA = true;
		m_aaMode = 5;

		//________Original ASAA________
		asaa->setIndex(index);

		asaa->setViewMatrix(viewMat4);
		asaa->setProjectionOffsetMatrix(currentProjectionNoG);

		asaa->apply(target, context, depth, noDepthTest, cullNone, linearClamp, camPos, width, height);

		context->CopyResource(backBuffer, ((Direct3D11Renderer*)renderer)->getResource(asaa->getRT()));
		renderer->changeToMainFramebuffer();
		//_____________________________

		////________Test of exponential smoothing________
		//asaaexp->setIndex(index);

		//asaaexp->setViewMatrix(viewMat4);
		//asaaexp->setProjectionOffsetMatrix(currentProjection);

		//asaaexp->apply(target, context, depth, noDepthTest, cullNone, camPos, width, height);

		//context->CopyResource(backBuffer, ((Direct3D11Renderer*)renderer)->getResource(asaaexp->getRT()));
		//renderer->changeToMainFramebuffer();
		////_________________________________________________


		////________Test of ASAA with reprojection of dynamic objects________
		//for (uint i = 0; i < dynamicModels.size(); i++)
		//{
		//	asaa->applyWVP(dynamicModels.at(i), target, context, depth, noDepthTest, cullNone, camPos, width, height);
		//}

		//renderer->changeRenderTarget(asaa->getRT(), depth);
		//// Skybox
		//m_SkyboxMdl->drawModel(m_DepthTest, cullFront, skyboxMat4, scale_bias);

		//context->CopyResource(backBuffer, ((Direct3D11Renderer*)renderer)->getResource(asaa->getRT()));
		//renderer->changeToMainFramebuffer();
		////______________________________________________________
	}
	else
	{
		//________Original ASAA________
		m_ASAA = false;

		asaa->subpixelBuffers.at(index).setViewMatrix(viewMat4);
		context->CopyResource(((Direct3D11Renderer*)renderer)->getResource(asaa->subpixelBuffers.at(index).m_SubpixelRT), ((Direct3D11Renderer*)renderer)->getResource(m_ResultRT));
		//_____________________________

		////________Test of exponential smoothing________
		//asaaexp->subpixelBuffers.at(index).setViewMatrix(viewMat4);
		//context->CopyResource(((Direct3D11Renderer*)renderer)->getResource(asaaexp->subpixelBuffers.at(index).m_SubpixelRT), ((Direct3D11Renderer*)renderer)->getResource(m_ResultRT));
		////_________________________________________________
	}

	if (m_UseMSAA->isChecked())
	{
		m_MSAA = true;
		m_aaMode = 6;

		context->ResolveSubresource(backBuffer, 0, ((Direct3D11Renderer*)renderer)->getResource(target), 0, DXGI_FORMAT_R8G8B8A8_UNORM);
		renderer->changeToMainFramebuffer();
	}
	else
		m_MSAA = false;

	//________Used by TAA________
	context->CopyResource(((Direct3D11Renderer*)renderer)->getResource(m_PrevResultRT), ((Direct3D11Renderer*)renderer)->getResource(t2msaa->getRT()));
	t2msaa->setPrevViewMatrix(viewMat4);
	t2msaa->setPrevProjectionMatrix(currentProjection);
	//___________________________

	// Used by the velocity calculations
	prevProjectionViewMat4 = projectionViewMat4;

	first = false;

	averageFrameTime += frameTime;
	index++;
	if (index > 3)
	{
		index = 0;
	}

	if (m_counter == 999)
	{
		writeStatistics(renderer, averageFrameTime, m_aaMode, 1000);

		averageFrameTime = 0.0f;
		m_counter = 0;
	}
	else
		m_counter++;

	//renderer->resetStatistics();

	if (m_saveSequence && (m_FrameCount - m_firstFrame) < 10) //Saves 10 screenshots in a sequence
		saveScreenshotDDS();

	// Check what multisampling levels the hardware supports
	/*for (uint sampleCount = 2; sampleCount <= 64; sampleCount)
	{
	uint numQualityLevels;
	device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, sampleCount, &numQualityLevels);

	char str[256];
	sprintf(str, "MSAA x%i supported? %i", sampleCount, numQualityLevels);

	outputDebugString(str);
	sampleCount *= 2;
	}*/
}
