/* * * * * * * * * * * * * Author's note * * * * * * * * * * * *\
*   _       _   _       _   _       _   _       _     _ _ _ _   *
*  |_|     |_| |_|     |_| |_|_   _|_| |_|     |_|  _|_|_|_|_|  *
*  |_|_ _ _|_| |_|     |_| |_|_|_|_|_| |_|     |_| |_|_ _ _     *
*  |_|_|_|_|_| |_|     |_| |_| |_| |_| |_|     |_|   |_|_|_|_   *
*  |_|     |_| |_|_ _ _|_| |_|     |_| |_|_ _ _|_|  _ _ _ _|_|  *
*  |_|     |_|   |_|_|_|   |_|     |_|   |_|_|_|   |_|_|_|_|    *
*                                                               *
*                     http://www.humus.name                     *
*                                                                *
* This file is a part of the work done by Humus. You are free to   *
* use the code in any way you like, modified, unmodified or copied   *
* into your own work. However, I expect you to respect these points:  *
*  - If you use this file and its contents unmodified, or use a major *
*    part of this file, please credit the author and leave this note. *
*  - For use in anything commercial, please request my approval.     *
*  - Share your work and ideas too as much as you can.             *
*                                                                *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Edited by Carl Stejmar

#ifndef _APPTAA_H_
#define _APPTAA_H_

#include "../Framework3/Direct3D11/D3D11App.h"

#include "../Framework3/Util/Model.h"
#include "../Framework3/Util/StaticModel.h"
#include "../Framework3/Util/DynamicModel.h"

#include "../Framework3/Util/GBAA.h"
#include "../Framework3/Util/FXAA.h"
#include "../Framework3/Util/FXAACS.h"
#include "../Framework3/Util/T2MSAA.h"
#include "../Framework3/Util/ASAA.h"

#include "../Framework3/Util/Velocity.h"

#include "../Framework3/Util/ASAAexp.h"

#include "WriteStatistics.h"

#include <string>
#include <vector>
#include <iostream>

struct Light
{
	float3 position;
	float radius;
};

const int LIGHT_COUNT = 10;

class App : public D3D11App
{
public:
	char *getTitle() const { return "Anti-Aliasing"; }

	void moveCamera(const float3 &dir);
	void resetCamera();

	bool onKey(const uint key, const bool pressed);
	void onSize(const int w, const int h);

	bool init();
	void exit();

	bool initAPI();
	void exitAPI();

	bool load();
	void unload();

	void drawFrame();

protected:
	const std::string c_shaderPath = "../../Shaders/";

	ShaderID m_FillBuffers[2], m_SkyBoxShd, m_AmbientShd, m_LightingShd, m_TextureShd;
	ShaderID m_Depth2MSDepthShd, m_MSAmbientShd, m_MSLightingShd;

	TextureID m_BaseRT, m_NormalRT, m_DepthRT, m_GeometryRT, m_ResultRT, m_PrevResultRT; // Normal render targets
	TextureID  m_MS2xResultRT, m_MS2xDepthRT, m_MS2xPrevDepthRT, m_MS2xBaseRT, m_MS2xNormalRT, m_MS2xPrevResultRT; // MS2X (MultiSampling) render targets
	TextureID  m_MS4xResultRT, m_MS4xDepthRT, m_MS4xBaseRT, m_MS4xNormalRT; // MS4X (MultiSampling) render targets
	TextureID  m_MS8xResultRT, m_MS8xDepthRT, m_MS8xBaseRT, m_MS8xNormalRT; // MS8X (MultiSampling) render targets
	//TextureID  m_MSResultRT, m_MSDepthRT, m_MSBaseRT, m_MSNormalRT; // MS (MultiSampling) render targets

	SamplerStateID m_BaseFilter, m_PointClamp;
	BlendStateID m_BlendAdd;
	DepthStateID m_DepthTest, m_DepthAlways, m_DepthTestLEQ;

	// Camera
	const float near_plane = 1.0f;
	const float far_plane = 2000.0f;
	mat4 projectionMat4;
	mat4 projectionViewMat4;
	mat4 prevProjectionViewMat4;

	// Light
	Light m_Lights[LIGHT_COUNT];
	Model *m_SphereMdl;

	//Static models
	StaticModel *m_SkyboxMdl;
	// Dynamic just for the ASAAwvp to work, otherwise static
	DynamicModel *m_TownMdl; //StaticModel *m_TownMdl;

	//Dynamic models
	DynamicModel *m_TRexMdl;
	DynamicModel *m_AppleMdl;
	DynamicModel *m_AppleMdl2;

	std::vector<StaticModel*> staticModels;
	std::vector<DynamicModel*> dynamicModels;

	// AA
	GBAA *gbaa;
	FXAA *fxaa;
	FXAACS *fxaacs;
	T2MSAA *t2msaa;
	ASAA *asaa;

	ASAAexp *asaaexp;

	// Velocity
	Velocity *velocity;

	int m_MS; // Number of multisamples
	int m_MSx;
	bool m_MultiSampling;
	bool first;
	int index;
	bool m_saveSequence;
	uint m_firstFrame;
	float averageFrameTime;
	uint m_aaMode;
	uint m_counter;

	CheckBox *m_UseGBAA;
	CheckBox *m_UseFXAA;
	CheckBox *m_UseFXAACS;
	CheckBox *m_UseTAA;
	CheckBox *m_UseASAA;
	CheckBox *m_UseMSAA;
};

#endif // _APPTAA_H_