//_________________________________________________________//
// File: WriteStatistics.h							       //
// Language: C++										   //
// Author: Carl Stejmar									   //
// Last edited: 2015-12-29								   //
// Description: Defines helper function for outputing	   //
// statistics to a text file							   //
//_________________________________________________________//

#include "../Framework3/Direct3D11/D3D11App.h"

#include <string>

void writeStatistics(Renderer* renderer, float frameTimeSum, uint aaMode, uint interval)
{
	std::string statStr = "";

	if (aaMode == 0)
		statStr += "_______________________No AA_______________________\n";
	else if (aaMode == 1)
		statStr += "_______________________GBAA_______________________\n";
	else if (aaMode == 2)
		statStr += "_______________________FXAA_______________________\n";
	else if (aaMode == 3)
		statStr += "_______________________FXAACS_______________________\n";
	else if (aaMode == 4)
		statStr += "_______________________T2MSAA2X_______________________\n";
	else if (aaMode == 5)
		statStr += "_______________________ASAA_______________________\n";
	else if (aaMode == 6)
		statStr += "_______________________MSAA_______________________\n";


	char str[256];
	sprintf(str, "Average frame time (s) over %u frames: %f\n", interval, frameTimeSum / (float)interval);
	statStr += str;

	char str2[256];
	sprintf(str2, "Number of vertices: %u\n", renderer->getVertexCount());
	statStr += str2;

	char str3[256];
	sprintf(str3, "Number of draw calls: %u\n", renderer->getDrawCallCount());
	statStr += str3;

	const char *statistics = { statStr.c_str() };

	FILE * pFile;
	pFile = fopen("statistics.txt", "a+");
	fwrite(statistics, sizeof(char), statStr.size(), pFile);
	fclose(pFile);

	renderer->resetStatistics();
}